package worms.model;

import java.util.ArrayList;

import be.kuleuven.cs.som.annotate.Basic;
import be.kuleuven.cs.som.annotate.Raw;

/**
 * A class of weapons, this is the super class of Rifle and Bazooka.
 * 
 * @invar  	Each weapon must have a proper worm.
 *       	| hasProperWorm()
 * 
 * @version  2.0
 * @author   Axel Lemmens & Maarten Rimaux
 */
public abstract class Weapon {
	
	/**
	 * Initialise this new weapon with the given parameters.
	 * 
	 * @param	worm
	 * 			The provided value for the worm of this new weapon.
	 * @post	The new worm of this new weapon is equal to the provided "worm"
	 * 			| new.getWorm() == worm
	 * @post   	The number of weapons for the given worm is
	 *         	incremented by 1.
	 *       	| (new worm).getWeapons().size() == worm.getWeapons().size() + 1
	 * @post   	The given worm has this new weapon as its very last weapon.
	 *      	| (new worm).getWeapons().get(worm.getWeapons().size()+1) == this
	 * @throws	IllegalArgumentException
	 * 			An exception is thrown when this new weapon cannot have the given worm as
	 *         	its worm.
	 * 			| ! canHaveAsWorm(worm)
	 */
	public Weapon(Worm worm) throws IllegalArgumentException{
		setWorm(worm);
		worm.addWeapon(this);
	}
	
	//--------------------- Name ----------------------------------//
	
	/**
	 *	A method to return the name of this weapon. 
	 */
	@Basic
	public abstract String getName();
	
	//--------------------- Worm ----------------------------------//

	/** 
	 * Return the worm of this weapon.
	 */
	@Basic @Raw 
	public Worm getWorm() {
		return this.worm;
	}
	
	/**
	 * Check whether this weapon can have the given worm as its current worm.
	 * 
	 * @param	worm
	 *          The worm to check.
	 * @return	True if and only if the given worm is effective.
	 *       	|   result == (worm != null) 
	 */
	@Raw
	public boolean canHaveAsWorm(Worm worm) {
			return (worm != null);
	}
	
	/**
	 * Check whether this weapon has a proper worm.
	 * 
	 * @return	True if and only if this weapon can have its worm as its
	 *          worm, and if the worm of this weapon has this weapon as 
	 *          one of its weapons.
	 *        	| result ==
	 *        	|   canHaveAsWorm(getWorm()) && getWorm().hasAsWeapon(this))
	 */
	public boolean hasProperWorm() {
		return canHaveAsWorm(getWorm()) && getWorm().hasAsWeapon(this);
	}
		
	/** 
	 * Register the given worm as the worm of this weapon.
	 * 
	 * @param	worm
	 *          The worm to be registered as the worm of this weapon.
	 * @post   	The worm of this weapon is the same as the given worm.
	 *       	| new.getWorm() == worm
	 * @throws	IllegalArgumentException
	 * 			An exception is thrown when the provided worm is not valid.
	 * 			| ! canHaveAsWorm(worm)
	 */
	@Raw
	private void setWorm(Worm worm) throws IllegalArgumentException {
		if( !canHaveAsWorm(worm) )
			throw new IllegalArgumentException("The provided worm is not a valid worm.");
		this.worm = worm;
	}
	
	/**
	 * Variable referencing the worm of this weapon.
	 */
	private Worm worm;
	
	//--------------------- PROJECTILES --------------------//
	
	/**
	 * A method that returns a list of all the projectiles of this weapon.
	 */
	@Basic
	public ArrayList<Projectile> getProjectiles() {
		return projectileObjects;
	}
	
	/**
	 * Check whether this weapon can have the given projectile
	 * as one of its projectiles.
	 * 
	 * @param	projectile
	 *        	The projectile to check.
	 * @return	True if and only if the given projectile is effective
	 *        	and already references this weapon, and this weapon
	 *        	does not yet have the given projectile as one of its projectiles.
	 *       	| result ==
	 *       	|   (projectile != null) && (projectile.getWeapon() == this)
	 *			|		&& (!this.hasAsProjectile(projectile))
	 */
	@Raw
	public boolean canHaveAsProjectile(Projectile projectile) {
		return (projectile != null) && (projectile.getWeapon() == this)
				&& (!this.hasAsProjectile(projectile));
	}
	
	/**
	 * Check whether this weapon has the given projectile as one of its
	 * projectiles.
	 * 
	 * @param  	projectile
	 * 		   	The projectile to check.
	 * @return	The result is true when the provided projectile is a projectile of this weapon.
	 *       	| result == getProjectiles().contains(projectile)
	 */
	public boolean hasAsProjectile(@Raw Projectile projectile) {
		return getProjectiles().contains(projectile);
	}
	
	/**
	 * Add the given projectile to the list of projectiles of this weapon.
	 * 
	 * @param	projectile
	 *         	The projectile to be added.
	 * @post	The number of projectiles of this weapon is
	 *        	incremented by 1.
	 *      	| new.getProjectiles().size() == this.getProjectiles().size() + 1
	 * @post   	This weapon has the given projectile as its very last projectile.
	 *       	| new.getProjectiles().get(getProjectiles().size()+1) == projectile
	 * @throws	IllegalArgumentException
	 * 			An exception is thrown when the provided projectile is not a valid projectile.
	 * 			| ! canHaveAsProjectile(projectile)
	 */
	@Basic
	public void addProjectile(Projectile projectile) throws IllegalArgumentException{
		if(!canHaveAsProjectile(projectile))
			throw new IllegalArgumentException("The provided projectile is not a valid projectile.");
		projectileObjects.add(projectile);
	}
	
	/**
	 * 	A method to remove the given projectile from the list of projectiles of this weapon.
	 * 
	 * @param	projectile
	 *         	The projectile to be removed.
	 * @post   	The number of projectiles of this weapon is
	 *         	decremented by 1.
	 *       	| new.getProjectiles().size() == getProjectiles().size() - 1
	 * @post    This weapon has no longer the given projectile as
	 *      	one of its projectiles.
	 *       	| ! new.hasAsProjectile(projectile)
	 * @post   	All projectiles registered at an index beyond the index at
	 *        	which the given projectile was registered, are shifted
	 *       	one position to the left.
	 *       	| for each I,J in 0..getProjectiles().size()-1:
	 *      	|   if ( (getProjectiles().get(I) == projectile) and (I < J) )
	 *      	|     then new.getProjectiles().get(J-1) == getProjectiles().get(J)
	 * @throws	IllegalArgumentException
	 * 			An exception is thrown when the projectile doesn't exists or
	 * 			if this weapon has not the given projectile as one of its projectile or
	 * 			if the given projectile his weapon still exists.
	 * 			| (projectile == null) || !this.hasAsProjectile(projectile) || !(projectile.getWeapon() == null)
	 */
	@Raw
	public void removeProjectile(Projectile projectile) throws IllegalArgumentException {
		if( (projectile == null) || !this.hasAsProjectile(projectile) || !(projectile.getWeapon() == null) )
			throw new IllegalArgumentException("The provided projectile is not a valid projectile.");
		getProjectiles().remove(projectile);
	}
	
	/**
	 * Variable referencing a list collecting all the projectiles of this weapon.
	 */
	private ArrayList<Projectile> projectileObjects = new ArrayList<Projectile>();
}