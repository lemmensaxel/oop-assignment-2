package worms.model;

import static org.junit.Assert.*;

import java.io.File;
import java.util.Random;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import worms.gui.Level;


public class Worm_test {
	

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {

	}

	@After
	public void tearDown() throws Exception {
	}
	
	// POSITION
	
	@Test
	public void isValidCoordinate_LegalCase() {

		
		assertEquals(true, Worm.isValidCoordinate(1.0));
	}
	
	@Test
	public void isValidCoordinate_IllegalCase() {
		
		assertEquals(true, Worm.isValidCoordinate(-1.0));
	}
	
	@Test
	public void setCoordinateX_LegalCase() throws IllegalArgumentException {
		Random random = new Random();
		Level level = new Level(new File("levels/Simple.lvl"));
		level.load();
		Facade facade = new Facade();
		World world = facade.createWorld(level.getWorldWidth(), level.getWorldHeight(), level.getPassableMap(), random);
		
		if (world.findRandomAdjacentLocation(0.50)) {
		}
		
		Worm testworm = new Worm(world.getAdjacentLocationX(), world.getAdjacentLocationY(), Math.PI/4, 0.50, "Kiki", world);
		
		testworm.setCoordinateX(8.0);
		
		assertEquals(8.0, testworm.getCoordinateX(), 0);
	}

	
	@Test
	public void setCoordinateY_LegalCase() throws IllegalArgumentException {
		Random random = new Random();
		Level level = new Level(new File("levels/Simple.lvl"));
		level.load();
		Facade facade = new Facade();
		World world = facade.createWorld(level.getWorldWidth(), level.getWorldHeight(), level.getPassableMap(), random);
		
		if (world.findRandomAdjacentLocation(0.50)) {
		}
		
		Worm testworm = new Worm(world.getAdjacentLocationX(), world.getAdjacentLocationY(), Math.PI/4, 0.50, "Kiki", world);
		
		testworm.setCoordinateY(10.0);
		
		assertEquals(10.0, testworm.getCoordinateY(), 0);
	}
	
	//DIRECTION
	
	@Test
	public void getDirection_Case() {
		Random random = new Random();
		Level level = new Level(new File("levels/Simple.lvl"));
		level.load();
		Facade facade = new Facade();
		World world = facade.createWorld(level.getWorldWidth(), level.getWorldHeight(), level.getPassableMap(), random);
		
		if (world.findRandomAdjacentLocation(0.50)) {
		}
		
		Worm testworm = new Worm(world.getAdjacentLocationX(), world.getAdjacentLocationY(), Math.PI/4, 0.50, "Kiki", world);
		
		assertEquals(Math.PI/4, testworm.getDirection(), 0.01);
	}
	
	
	@Test
	public void isValidDirection_LegalCase() {
		
		assertEquals(true, Worm.isValidDirection(1.0));
	}
	
	
	@Test
	public void setDirection_LegalCase() {
		Random random = new Random();
		Level level = new Level(new File("levels/Simple.lvl"));
		level.load();
		Facade facade = new Facade();
		World world = facade.createWorld(level.getWorldWidth(), level.getWorldHeight(), level.getPassableMap(), random);
		
		if (world.findRandomAdjacentLocation(0.50)) {
		}
		
		Worm testworm = new Worm(world.getAdjacentLocationX(), world.getAdjacentLocationY(), Math.PI/4, 0.50, "Kiki", world);
		testworm.setDirection(Math.PI/8);
		
		assertEquals(Math.PI/8, testworm.getDirection(), 0);
	}
	
	//RADIUS
	
	@Test
	public void isValidRadius_LegalCase() {
		Random random = new Random();
		Level level = new Level(new File("levels/Simple.lvl"));
		level.load();
		Facade facade = new Facade();
		World world = facade.createWorld(level.getWorldWidth(), level.getWorldHeight(), level.getPassableMap(), random);
		if (world.findRandomAdjacentLocation(0.50)) {
		}
		
		Worm testworm = new Worm(world.getAdjacentLocationX(), world.getAdjacentLocationY(), Math.PI/4, 0.50, "Kiki", world);
		
		assertTrue(testworm.isValidRadius(1.0));
	}
	
	@Test
	public void isValidRadius_IllegalCase() {
		Random random = new Random();
		Level level = new Level(new File("levels/Simple.lvl"));
		level.load();
		Facade facade = new Facade();
		World world = facade.createWorld(level.getWorldWidth(), level.getWorldHeight(), level.getPassableMap(), random);
		
		if (world.findRandomAdjacentLocation(0.50)) {
		}
		
		Worm testworm = new Worm(world.getAdjacentLocationX(), world.getAdjacentLocationY(), Math.PI/4, 0.50, "Kiki", world);
		
		assertFalse(testworm.isValidRadius(0.21));
	}
	
	@Test
	public void isValidRadius_IllegalCase2() {
		Random random = new Random();
		Level level = new Level(new File("levels/Simple.lvl"));
		level.load();
		Facade facade = new Facade();
		World world = facade.createWorld(level.getWorldWidth(), level.getWorldHeight(), level.getPassableMap(), random);
		
		if (world.findRandomAdjacentLocation(0.50)) {
		}
		
		Worm testworm = new Worm(world.getAdjacentLocationX(), world.getAdjacentLocationY(), Math.PI/4, 0.50, "Kiki", world);
		
		assertFalse(testworm.isValidRadius(-0.25));
	}
	
	@Test
	public void setRadius_LegalCase() throws IllegalArgumentException {
		Random random = new Random();
		Level level = new Level(new File("levels/Simple.lvl"));
		level.load();
		Facade facade = new Facade();
		World world = facade.createWorld(level.getWorldWidth(), level.getWorldHeight(), level.getPassableMap(), random);
		
		if (world.findRandomAdjacentLocation(0.50)) {
		}
		
		Worm testworm = new Worm(world.getAdjacentLocationX(), world.getAdjacentLocationY(), Math.PI/4, 0.50, "Kiki", world);
		
		testworm.setRadius(0.53);
		
		assertEquals(0.53, testworm.getRadius(), 0);
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void setRadius_IllegalCase() throws IllegalArgumentException {
		Random random = new Random();
		Level level = new Level(new File("levels/Simple.lvl"));
		level.load();
		Facade facade = new Facade();
		World world = facade.createWorld(level.getWorldWidth(), level.getWorldHeight(), level.getPassableMap(), random);
		
		if (world.findRandomAdjacentLocation(0.50)) {
		}
		
		Worm testworm = new Worm(world.getAdjacentLocationX(), world.getAdjacentLocationY(), Math.PI/4, 0.50, "Kiki", world);
		
		testworm.setRadius(-1.0);
		
	}
	
	//MASS
	
	@Test
	public void getMass_Case() {
		Random random = new Random();
		Level level = new Level(new File("levels/Simple.lvl"));
		level.load();
		Facade facade = new Facade();
		World world = facade.createWorld(level.getWorldWidth(), level.getWorldHeight(), level.getPassableMap(), random);
		
		if (world.findRandomAdjacentLocation(0.50)) {
		}
		
		Worm testworm = new Worm(world.getAdjacentLocationX(), world.getAdjacentLocationY(), Math.PI/4, 0.50, "Kiki", world);
		
		assertEquals(556, testworm.getMass(), 1);
		
	}
	
	// ACTION POINTS
	
	@Test
	public void getMaximumActionPoints_Case() {
		Random random = new Random();
		Level level = new Level(new File("levels/Simple.lvl"));
		level.load();
		Facade facade = new Facade();
		World world = facade.createWorld(level.getWorldWidth(), level.getWorldHeight(), level.getPassableMap(), random);
		
		if (world.findRandomAdjacentLocation(0.50)) {
		}
		
		Worm testworm = new Worm(world.getAdjacentLocationX(), world.getAdjacentLocationY(), Math.PI/4, 0.50, "Kiki", world);
		
		assertEquals(556,testworm.getMaximumActionPoints());
		
		
	}
	
	@Test
	public void getCurrentActionPoints_Case() {
		Random random = new Random();
		Level level = new Level(new File("levels/Simple.lvl"));
		level.load();
		Facade facade = new Facade();
		World world = facade.createWorld(level.getWorldWidth(), level.getWorldHeight(), level.getPassableMap(), random);
		
		if (world.findRandomAdjacentLocation(0.50)) {
		}
		
		Worm testworm = new Worm(world.getAdjacentLocationX(), world.getAdjacentLocationY(), Math.PI/4, 0.50, "Kiki", world);
		
		assertEquals(556, testworm.getCurrentActionPoints());
	}
	
	@Test
	public void setCurrentActionPoints_LegalCase1() {
		Random random = new Random();
		Level level = new Level(new File("levels/Simple.lvl"));
		level.load();
		Facade facade = new Facade();
		World world = facade.createWorld(level.getWorldWidth(), level.getWorldHeight(), level.getPassableMap(), random);
		
		if (world.findRandomAdjacentLocation(0.50)) {
		}
		
		Worm testworm = new Worm(world.getAdjacentLocationX(), world.getAdjacentLocationY(), Math.PI/4, 0.50, "Kiki", world);
		
		testworm.setCurrentActionPoints(500);
		
		assertEquals(500, testworm.getCurrentActionPoints());
		
	}

	@Test
	public void setCurrentActionPoints_LegalCase3() { 
		Random random = new Random();
		Level level = new Level(new File("levels/Simple.lvl"));
		level.load();
		Facade facade = new Facade();
		World world = facade.createWorld(level.getWorldWidth(), level.getWorldHeight(), level.getPassableMap(), random);
		
		if (world.findRandomAdjacentLocation(0.50)) {
		}
		
		Worm testworm = new Worm(world.getAdjacentLocationX(), world.getAdjacentLocationY(), Math.PI/4, 0.50, "Kiki", world);
		
		testworm.setCurrentActionPoints(-500);
		assertEquals(0, testworm.getCurrentActionPoints());
	}
	
	//NAME
	
	@Test
	public void getName_Case() {
		Random random = new Random();
		Level level = new Level(new File("levels/Simple.lvl"));
		level.load();
		Facade facade = new Facade();
		World world = facade.createWorld(level.getWorldWidth(), level.getWorldHeight(), level.getPassableMap(), random);
		
		if (world.findRandomAdjacentLocation(0.50)) {
		}
		
		Worm testworm = new Worm(world.getAdjacentLocationX(), world.getAdjacentLocationY(), Math.PI/4, 0.50, "Kiki", world);
		
		assertEquals("Kiki", testworm.getName());
	}
	
	@Test
	public void isValidName_LegalCase() {
		
		assertEquals(true, Worm.isValidName("Axel"));
	}
	
	@Test
	public void isValidName_IllegalCase1() {
		
		assertEquals(false, Worm.isValidName("axel"));
	}
	
	@Test
	public void isValidName_IllegalCase2() {
		
		assertEquals(false, Worm.isValidName("Axel^$$"));
	}
	
	@Test
	public void isValidName_IllegalCase3() {
		
		assertEquals(true, Worm.isValidName("Ax"));
	}
	
	@Test
	public void isValidName_IllegalCase4() {
		
		assertEquals(false, Worm.isValidName("34254"));
	}

	@Test
	public void isValidName_IllegalCase5() {
		
		assertEquals(true, Worm.isValidName("A\"xe\"l"));
	}
	
	@Test
	public void isValidName_IllegalCase6() {
		
		assertEquals(true, Worm.isValidName("Axel45678"));
	}
	
	@Test
	public void isValidName_IllegalCase7() {
		
		assertEquals(false, Worm.isValidName("^$^"));
	}
	
	@Test
	public void isValidName_IllegalCase8() {
		
		assertEquals(false, Worm.isValidName("A"));
	}
	
	@Test
	public void isValidName_IllegalCase9() {
		
		assertEquals(false, Worm.isValidName(""));
	}
	
	@Test
	public void setName_LegalCase() {
		Random random = new Random();
		Level level = new Level(new File("levels/Simple.lvl"));
		level.load();
		Facade facade = new Facade();
		World world = facade.createWorld(level.getWorldWidth(), level.getWorldHeight(), level.getPassableMap(), random);
		
		if (world.findRandomAdjacentLocation(0.50)) {
		}
		
		Worm testworm = new Worm(world.getAdjacentLocationX(), world.getAdjacentLocationY(), Math.PI/4, 0.50, "Kiki", world);
		
		testworm.setName("ObjectOrientedProgrammer");
		
		assertEquals("ObjectOrientedProgrammer", testworm.getName());
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void setName_IllegalCase() throws IllegalArgumentException {
		Random random = new Random();
		Level level = new Level(new File("levels/Simple.lvl"));
		level.load();
		Facade facade = new Facade();
		World world = facade.createWorld(level.getWorldWidth(), level.getWorldHeight(), level.getPassableMap(), random);
		
		if (world.findRandomAdjacentLocation(0.50)) {
		}
		
		Worm testworm = new Worm(world.getAdjacentLocationX(), world.getAdjacentLocationY(), Math.PI/4, 0.50, "Kiki", world);
		
		testworm.setName("objectOrientedProgrammer");
	}
	
	//MOVING
	
	@Test
	public void hasEnoughActionPoints_LegalCase() {
		Random random = new Random();
		Level level = new Level(new File("levels/Simple.lvl"));
		level.load();
		Facade facade = new Facade();
		World world = facade.createWorld(level.getWorldWidth(), level.getWorldHeight(), level.getPassableMap(), random);
		
		if (world.findRandomAdjacentLocation(0.50)) {
		}
		
		Worm testworm = new Worm(world.getAdjacentLocationX(), world.getAdjacentLocationY(), Math.PI/4, 0.50, "Kiki", world);
		testworm.setCurrentActionPoints(500);
		assertEquals(true, testworm.hasEnoughActionPoints(1));
		
	}
	
	@Test
	public void hasEnoughActionPoints_IllegalCase() {
		Random random = new Random();
		Level level = new Level(new File("levels/Simple.lvl"));
		level.load();
		Facade facade = new Facade();
		World world = facade.createWorld(level.getWorldWidth(), level.getWorldHeight(), level.getPassableMap(), random);
		
		if (world.findRandomAdjacentLocation(0.50)) {
		}
		
		Worm testworm = new Worm(world.getAdjacentLocationX(), world.getAdjacentLocationY(), Math.PI/4, 0.50, "Kiki", world);
		
		testworm.setCurrentActionPoints(0);
		assertEquals(false, testworm.hasEnoughActionPoints(5));
		
	}
	
	// TURN
	
	@Test
	public void turn_LegalCase() {
		Random random = new Random();
		Level level = new Level(new File("levels/Simple.lvl"));
		level.load();
		Facade facade = new Facade();
		World world = facade.createWorld(level.getWorldWidth(), level.getWorldHeight(), level.getPassableMap(), random);
		
		if (world.findRandomAdjacentLocation(0.50)) {
		}
		
		Worm testworm = new Worm(world.getAdjacentLocationX(), world.getAdjacentLocationY(), Math.PI/4, 0.50, "Kiki", world);
		testworm.setCurrentActionPoints(500);
		double direction = testworm.getDirection();
		testworm.turn(2);
		assertEquals(direction+2, testworm.getDirection(), 0.1);
		assertEquals(500-19.01, testworm.getCurrentActionPoints(), 1);
		
	}
	
	@Test
	public void canTurn_LegalCase() {
		Random random = new Random();
		Level level = new Level(new File("levels/Simple.lvl"));
		level.load();
		Facade facade = new Facade();
		World world = facade.createWorld(level.getWorldWidth(), level.getWorldHeight(), level.getPassableMap(), random);
		
		if (world.findRandomAdjacentLocation(0.50)) {
		}
		
		Worm testworm = new Worm(world.getAdjacentLocationX(), world.getAdjacentLocationY(), Math.PI/4, 0.50, "Kiki", world);
		
		testworm.setCurrentActionPoints(500);
		assertEquals(true, testworm.canTurn(2));
	}
	
	@Test
	public void canTurn_IllegalCase() {
		Random random = new Random();
		Level level = new Level(new File("levels/Simple.lvl"));
		level.load();
		Facade facade = new Facade();
		World world = facade.createWorld(level.getWorldWidth(), level.getWorldHeight(), level.getPassableMap(), random);
		
		if (world.findRandomAdjacentLocation(0.50)) {
		}
		
		Worm testworm = new Worm(world.getAdjacentLocationX(), world.getAdjacentLocationY(), Math.PI/4, 0.50, "Kiki", world);
		
		testworm.setCurrentActionPoints(0);
		assertEquals(false, testworm.canTurn(2));
	}
	
	// JUMP
	
	@Test
	public void getForce_Case() {
		Random random = new Random();
		Level level = new Level(new File("levels/Simple.lvl"));
		level.load();
		Facade facade = new Facade();
		World world = facade.createWorld(level.getWorldWidth(), level.getWorldHeight(), level.getPassableMap(), random);
		
		if (world.findRandomAdjacentLocation(0.50)) {
		}
		
		Worm testworm = new Worm(world.getAdjacentLocationX(), world.getAdjacentLocationY(), Math.PI/4, 0.50, "Kiki", world);
		
		testworm.setCurrentActionPoints(100);
		assertEquals(5953, testworm.getForce(), 1);
		
	}
	
	@Test
	public void getVelocity_Case() {
		Random random = new Random();
		Level level = new Level(new File("levels/Simple.lvl"));
		level.load();
		Facade facade = new Facade();
		World world = facade.createWorld(level.getWorldWidth(), level.getWorldHeight(), level.getPassableMap(), random);
		
		if (world.findRandomAdjacentLocation(0.50)) {
		}
		
		Worm testworm = new Worm(world.getAdjacentLocationX(), world.getAdjacentLocationY(), Math.PI/4, 0.50, "Kiki", world);
		
		assertEquals(7.4, testworm.getVelocity(), 1);
	}
	
	@Test
	public void canJump_LegalCase() {
		Random random = new Random();
		Level level = new Level(new File("levels/Simple.lvl"));
		level.load();
		Facade facade = new Facade();
		World world = facade.createWorld(level.getWorldWidth(), level.getWorldHeight(), level.getPassableMap(), random);
		
		if (world.findRandomAdjacentLocation(0.50)) {
		}
		
		Worm testworm = new Worm(world.getAdjacentLocationX(), world.getAdjacentLocationY(), Math.PI/4, 0.50, "Kiki", world);
		
		testworm.setCurrentActionPoints(100);
		assertEquals(true, testworm.canJump());
	}
	
	@Test
	public void canJump_IllegalCase() {
		Random random = new Random();
		Level level = new Level(new File("levels/Simple.lvl"));
		level.load();
		Facade facade = new Facade();
		World world = facade.createWorld(level.getWorldWidth(), level.getWorldHeight(), level.getPassableMap(), random);
		
		if (world.findRandomAdjacentLocation(0.50)) {
		}
		
		Worm testworm = new Worm(world.getAdjacentLocationX(), world.getAdjacentLocationY(), Math.PI/4, 0.50, "Kiki", world);
		
		assertEquals(true, testworm.canJump());
		
	}
	
	@Test
	public void canJump_IllegalCase2() {
		Random random = new Random();
		Level level = new Level(new File("levels/Simple.lvl"));
		level.load();
		Facade facade = new Facade();
		World world = facade.createWorld(level.getWorldWidth(), level.getWorldHeight(), level.getPassableMap(), random);
		
		if (world.findRandomAdjacentLocation(0.50)) {
		}
		
		Worm testworm = new Worm(world.getAdjacentLocationX(), world.getAdjacentLocationY(), Math.PI/4, 0.50, "Kiki", world);
		
		assertEquals(true, testworm.canJump());
		
	}


	
	@Test
	public void canJump_IllegalCase3() {
		Random random = new Random();
		Level level = new Level(new File("levels/Simple.lvl"));
		level.load();
		Facade facade = new Facade();
		World world = facade.createWorld(level.getWorldWidth(), level.getWorldHeight(), level.getPassableMap(), random);
		
		if(world.findRandomAdjacentLocation(0.50)){
		}
		Worm testworm = new Worm(world.getAdjacentLocationX(), world.getAdjacentLocationY(), Math.PI/4, 0.50, "Kiki", world);
		
		assertEquals(true, testworm.canJump());
		
	}

}