package worms.model;

import java.util.ArrayList;

import be.kuleuven.cs.som.annotate.Basic;
import be.kuleuven.cs.som.annotate.Immutable;
import be.kuleuven.cs.som.annotate.Raw;

/**
 * A class of worms.
 * 
 * @invar	Each worm must have a valid x-coordinate.
 * 			| isValidCoordinate(getCoordinateX())
 * @invar 	Each worm must have a valid y-coordinate.
 * 			| isValidCoordinate(getCoordinateY())
 * @invar	Each worm must have a valid direction.
 * 			| isValidDirection(getDirection())
 * @invar	Each worm must have a valid radius.
 * 			| isValidRadius(getRadius())
 * @invar	Each worm must have a valid name.
 * 			| isValidName(getName())
 * @invar  	Each worm must have a proper world.
 *       	| hasProperWorld()
 *
 * @version	2.0
 * @author Axel Lemmens & Maarten Rimaux 
 *
 */
public class Worm {
	
	//CONSTRUCTOR
	
	/**
	 * @param 		xCoordinate
	 * 				The provided value for the x-coordinate in meters.
	 * @param 		yCoordinate
	 * 				The provided value for the y-coordinate in meters.
	 * @param 		direction
	 * 				The provided value for the direction in radians.
	 * @param 		radius
	 * 				The provided value for the radius in meter.
	 * @param 		name
	 * 				The provided string for the name.
	 * @pre			The provided direction must be a valid value.
	 * 				| isValidDirection(direction)
	 * @effect		The new value of the x-coordinate is set to the provided value "xCoordinate"
	 * 				| setCoordinateX(xCoordinate)
	 * @effect		The new value of the y-coordinate is set to the provided value "yCoordinate"
	 * 				| setCoordinateY(yCoordinate)
	 * @effect		The new value of the direction is set to the provided value "direction"
	 * 				| setDirection(direction)
	 * @effect		The new value of the radius is set to the provided value "radius"
	 * 				| setRadius(radius)
	 * @effect		The new value of the name is set to the provided string "name"
	 * 				| setName(name)
	 * @effect		the new value of the action points is set to the maximum action points
	 * 				| setCurrentActionPoints(getMaximumActionPoints())
	 * @effect		the new value of the hit points is set to the maximum hit points
	 * 				| setCurrentHitPoints(getMaximumHitPoints())
	 * @effect		The new world of this worm is set to the provided "world"
	 * 				| setWorld(world)
	 * @effect		The next weapon is selected
	 * 				| new.getSelectedWeapon = getWeapons().get(0)
	 * @post   		The number of worms for the given world is
	 *         		incremented by 1.
	 *       		| (new world).getWorms().size() == world.getWorms().size() + 1
	 * @post   		The given world has this new worm as its very last worm.
	 *      		| (new world).getWorms().get(world.getWorms().size()+1) == this
	 * @post		The number of weapons for this worm is incremented by 1 with the rifle gun.
	 * 				| (new worm).getWeapons().size() == this.getWeapons().size()+1
	 * @post		The number of weapons for this worm is incremented by 1 with the bazooka gun.
	 * 				| (new worm).getWeapons().size() == this.getWeapons().size()+1
	 * @throws		IllegalArgumentException
	 * 				An exception is thrown when the provided value is not a valid coordinate.
	 * 				| ! isValidCoordinate(xCoordinate)
	 * @throws		IllegalArgumentException
	 * 				An exception is thrown when the provided value is not a valid coordinate.
	 * 				| ! isValidCoordinate(yCoordinate)
	 * @throws		IllegalArgumentException
	 * 				An exception is thrown when the provided value is not a valid radius.
	 * 				| ! isValidRadius(radius)
	 * @throws		IllegalArgumentException
	 * 				An exception is thrown when the provided value is not a valid name.
	 * 				| ! isValidName(name)
	 * @throws		IllegalArgumentException
	 * 				An exception is thrown when the provided world is not a valid world.
	 * 				| ! canHaveAsWorld(world)
	 */
	public Worm(double xCoordinate, double yCoordinate, double direction, double radius, String name, World world) throws IllegalArgumentException {		
		this.setCoordinateX(xCoordinate);
		this.setCoordinateY(yCoordinate);
		this.setDirection(direction);
		this.setRadius(radius);
		this.setName(name);
		this.setCurrentActionPoints(getMaximumActionPoints());
		this.setCurrentHitPoints(getMaximumHitPoints());
		this.setWorld(world);
		world.addWorm(this);
		this.setTeam(null);
		Rifle rifle = new Rifle(this);
		Bazooka bazooka = new Bazooka(this);
		selectNextWeapon();
	}
	
	//POSITION
	
	/**
	 * A method to return the x-coordinate of the current position of this worm.
	 */
	@Basic
	public double getCoordinateX() {
		return xCoordinate;
	}
	
	/**
	 * A method to return the y-coordinate of the current position of this worm.
	 */
	@Basic
	public double getCoordinateY() {
		return yCoordinate;
	}
	
	/**
	 * A method to set the x-coordinate of the position of this worm.
	 * 
	 * @param 		xCoordinate
	 * 				The new x-coordinate of the position of this worm.
	 * @post		The new value of the xCoordinate is set to the provided value.
	 * 				| new.getCoordinateX() == xCoordinate
	 * @throws		IllegalArgumentException
	 * 				An exception is thrown when the provided value is not a valid coordinate.
	 * 				| ! isValidCoordinate(xCoordinate) 
	 */
	public void setCoordinateX(double xCoordinate) throws IllegalArgumentException {
		if (! isValidCoordinate(xCoordinate))
			throw new IllegalArgumentException();
		this.xCoordinate = xCoordinate;
	}
	
	/**
	 * A method to set the y-coordinate of the position of this worm.
	 * 
	 * @param 		yCoordinate
	 * 				The new y-coordinate of the position of a worm.
	 * @post		The new value of the yCoordinate is set to the provided value.
	 * 				| new.getCoordinateY() == yCoordinate
	 * @throws		IllegalArgumentException
	 * 				An exception is thrown when the provided value is not a valid coordinate.
	 * 				| ! isValidCoordinate(yCoordinate)
	 */
	public void setCoordinateY(double yCoordinate) throws IllegalArgumentException { 
		if (! isValidCoordinate(yCoordinate))
			throw new IllegalArgumentException();	
		this.yCoordinate = yCoordinate;
		
	}
	
	/**
	 * A method to check whether a coordinate is valid.
	 * 
	 * @param 		aCoordinate
	 * 				The coordinate to check.
	 * @return 		The result should be a valid number.
	 * 				| result == !double.isNaN(aCoordinate) 
	 * 				| && (aCoordinate < Double.POSITIVE_INFINITY)
	 * 				| && (aCoordinate > Double.NEGATIVE_INFINITY)
	 */
	public static boolean isValidCoordinate(double aCoordinate) { 
		return (!Double.isNaN(aCoordinate) 
				&& (aCoordinate < Double.POSITIVE_INFINITY) 
				&& (aCoordinate > Double.NEGATIVE_INFINITY));									 
	}
	
	/**
	 * Variable registering the x-coordinate of the position of this worm.
	 */
	private double xCoordinate;
	
	/**
	 * Variable registering the y-coordinate of the position of this worm.
	 */
	private double yCoordinate;
	
	// ORIENTATION / DIRECTION
	
	/**
	 * A method to return the current value of the angel of this worm.
	 */
	@Basic
	public double getDirection() {
		return direction;
	}
	
	/**
	 * A method to set the direction of this worm to the provided value.
	 * 
	 * @param 		direction
	 * 				The new direction for the orientation of this worm.
	 * @pre			The provided value must be a valid value.
	 * 				| isValidDirection(direction)
	 * @post		The direction of this worm is set to the provided value.
	 * 				| new.getDirection() == direction
	 */
	public void setDirection(double direction) { 
		assert(isValidDirection(direction));
		this.direction = direction;
	}
	
	/**
	 * A method to check whether a direction is valid.
	 * 
	 * @param 		direction 
	 * 				The direction to check.
	 * @return 		The result should be a valid number.
	 * 				| result == !double.isNaN(direction) 
	 * 				| && (direction < Double.POSITIVE_INFINITY) 
	 * 				| && (direction > Double.NEGATIVE_INFINITY)
	 */
	public static boolean isValidDirection(double direction){
		return (!Double.isNaN(direction) 
				&& (direction < Double.POSITIVE_INFINITY) 
				&& (direction > Double.NEGATIVE_INFINITY));	
	}
	
	/**
	 * Variable registering the direction for the orientation of this worm.
	 */
	private double direction;
	
	// RADIUS
	
	/**
	 * A method to return the current radius of this worm expressed in meters.
	 */
	@Basic
	public double getRadius() {
		return radius;
	}
	
	/**
	 * A method to return the lower bound radius of this worm expressed in meters.
	 */
	@Basic
	public double getLowerBoundRadius() {
		return lowerBoundRadius;
	}
	
	/**
	 * A method that sets the radius of this worm to the provided value.
	 * 
	 * @param 		radius
	 * 				The new radius of this worm.
	 * @post 		The radius of this worm is set to the provided value.
	 * 				| new.getRadius() == radius
	 * @post		IllegalArgumentException
	 * 				The method must throw an exception when the provided value is not a valid radius.
	 * 				| !isValidRadius(radius)
	 */
	public void setRadius(double radius) throws IllegalArgumentException {
		if (! isValidRadius(radius))
			throw new IllegalArgumentException();
		this.radius = radius;
	}
	
	/**
	 * A method to check whether a given value for radius is valid.
	 * 
	 * @param		radius
	 * 				The radius to check.
	 * @return		The result should be larger or equal to the lower bound of the radius.
	 * 				| result ==  (radius >= getLowerBoundRadius())
	 * @return 		The result must be a valid number.
	 * 				| result == double.isNaN(radius) 
	 * 				| && (radius < Double.POSITIVE_INFINITY)
	 * 				| && (radius > Double.NEGATIVE_INFINITY))
	 */
	public Boolean isValidRadius(double radius) {
		return (radius >= this.getLowerBoundRadius())
				&& (!Double.isNaN(radius) 
				&& (radius < Double.POSITIVE_INFINITY)
				&& (radius > Double.NEGATIVE_INFINITY));
	}
	
	/**
	 * Variable registering the radius of this worm.
	 */
	private double radius;
	
	/**
	 * Variable registering the lower bound radius of this worm.
	 */
	private double lowerBoundRadius = 0.25;
	
	//  MASS
	
	/**
	 * A method to return the mass of a worm expressed in kilogram.
	 * 
	 * @return 		The mass of this worm.
	 * 				| result ==	
	 * 				| this.getDensity()*((4.0/3.0)*(Math.PI)*(Math.pow(this.getRadius(),3)))
	 */
	public double getMass() {
		return (this.getDensity()*((4.0/3.0)*(Math.PI)*(Math.pow(this.getRadius(),3))));
	}
	
	/**
	 * A method to return the density of this worm.
	 */
	@Basic @Immutable
	public double getDensity(){
		return 1062.0;
	}
	
	// ACTION POINTS
	
	/**
	 * A method to return the maximum amount of action points of this worm.
	 * 
	 * @return 		The maximum amount of action of this worm.
	 * 				| result == (int)Math.round(this.getMass())
	 */
	public int getMaximumActionPoints() {
		return (int) Math.round(this.getMass());
	}
	
	/**
	 * A method to return the minimum amount of action points of this worm.
	 */
	@Basic	@Immutable
	public int getMinimumActionPoints() {
		return 0;
	}
	
	/**
	 * A method to return the current amount of action points of this worm.
	 */
	@Basic
	public int getCurrentActionPoints() {
		return currentActionPoints;
	}
	
	/**
	 * A method to set the current action points on this worm to the provided value.
	 * 
	 * @param 		currentPoints
	 * 				The new current points for the action points of this worm.
	 * @post		If currentPoints is smaller or equal to the maximum value of points 
	 * 				and if currentPoints is larger or equal to the minimum value of points
	 * 				then currentPoints is set to the provided value.
	 * 				|if ((currentPoints <= getMaximumActionPoints()) 
	 * 				| && (currentPoints >= getMinimumActionPoints()))
	 * 				|	then new.getCurrentActionPoints() == currentPoints
	 * @post		If currentPoints is bigger than the maximum amount of action points, 
	 * 				currentPoints is set to the maximum amount of points.
	 * 				|if (currentPoints > getMaximumActionPoints())
	 * 				| 	then new.getCurrentActionPoints() == this.getMaximumActionPoints()
	 * @post		If the currentPoint is smaller than the minimum amount of action points, 
	 * 				currentPoints is set to the minimum amount of points.
	 * 				|if (currentPoints < getMinimumActionPoints())
	 * 				|	then new.getCurrentActionPoints() == this.getMinimumActionPoints()
	 */
	public void setCurrentActionPoints(int currentPoints) {
		if ( (currentPoints <= getMaximumActionPoints()) && (currentPoints >= getMinimumActionPoints()) )
				this.currentActionPoints = currentPoints;
		
		else if (currentPoints > getMaximumActionPoints()) {
				this.currentActionPoints = getMaximumActionPoints();
				}
		
		else if (currentPoints < getMinimumActionPoints()) {
				this.currentActionPoints =  getMinimumActionPoints();
				}
	}
	
	/**
	 * Variable registering the current action points of this worm.
	 */
	private int currentActionPoints;
	
	// HIT POINTS
	
	/**
	 * A method to return the maximum amount of hit points of this worm.
	 * 
	 * @return 		The maximum amount of hit points of this worm.
	 * 				| result == (int)Math.round(this.getMass())
	 */
	public int getMaximumHitPoints() {
		return (int) Math.round(this.getMass());
	}
	
	/**
	 * A method to return the minimum amount of hit points of this worm.
	 */
	@Basic	@Immutable
	public int getMinimumHitPoints() {
		return 0;
	}
	
	/**
	 * A method to return the current amount of hit points of this worm.
	 */
	@Basic
	public int getCurrentHitPoints() {
		return currentHitPoints;
	}
	
	/**
	 * A method to set the current hit points on this worm to the provided value.
	 * 
	 * @param 		currentPoints
	 * 				The new current points for the hit points of this worm.
	 * @post		If currentPoints is smaller or equal to the maximum value of points 
	 * 				and if currentPoints is larger or equal to the minimum value of points
	 * 				then currentPoints is set to the provided value.
	 * 				|if ((currentPoints <= getMaximumHitPoints()) 
	 * 				| && (currentPoints >= getMinimumHitPoints()))
	 * 				|	then new.getCurrentHitPoints() == currentPoints
	 * @post		If currentPoints is bigger than the maximum amount of hit points, 
	 * 				currentPoints is set to the maximum amount of points.
	 * 				|if (currentPoints > getMaximumHitPoints())
	 * 				| 	then new.getCurrentHitPoints() == this.getMaximumHitPoints()
	 * @post		If the currentPoint is smaller than the minimum amount of hit points, 
	 * 				currentPoints is set to the minimum amount of points.
	 * 				|if (currentPoints < getMinimumHitPoints())
	 * 				|	then new.getCurrentHitPoints() == this.getMinimumHitPoints()
	 */
	public void setCurrentHitPoints(int currentPoints) {
		if ( (currentPoints <= getMaximumHitPoints()) && (currentPoints >= getMinimumHitPoints()) )
				this.currentHitPoints = currentPoints;
		
		else if (currentPoints > getMaximumHitPoints()) {
				this.currentHitPoints = getMaximumHitPoints();
				}
		
		else if (currentPoints < getMinimumHitPoints()) {
				this.currentHitPoints =  getMinimumHitPoints();
				}
	}
	
	/**
	 * Variable registering the current hit points of this worm.
	 */
	private int currentHitPoints;
	
	// NAME
	
	/**
	 * A method that returns the current name of this worm.
	 */
	@Basic
	public String getName() {
		return name;
	}
	
	/**
	 * A method to set the name to a given string.
	 * 
	 * @param		name
	 * 				The new name of this worm.
	 * @post		The name of this worm is set to the provided value.
	 * 				| new.getName() == name
	 * @throws		IllegalArgumentException
	 * 				The method throws the exception when the provided string is not valid.
	 * 				| (! isValidName(name))
	 */
	public void setName(String name) throws IllegalArgumentException {
		if ( ! isValidName(name))
			throw new IllegalArgumentException();
		this.name = name;
		
	}
	
	/**
	 * A method to check whether a given string for name is valid.
	 * 
	 * @param 		name
	 * 				The name to check.
	 * @return		The result should be a string starting with an uppercase letter, 
	 * 				at least two characters long and contains only letters, numbers, quotes and spaces. 
	 * 				| result ==  (string.matches("^[A-Z][a-zA-Z0-9\\s\'\"]{1,}")
	 */
	public static boolean isValidName(String name) {
		return name.matches("^[A-Z][a-zA-Z0-9\\s\'\"]{1,}");
	}
	
	/**
	 * Variable registering the name of this worm.
	 */
	private String name;
	
	//MOVING
	
	/**
	 * A method to return the founded x-coordinate in the method move.
	 */
	public double getFoundedXCoordinate(){
		return foundedXCoordinate;
	}
	
	/**
	 * A method to return the founded y-coordinate in the method move.
	 */
	public double getFoundedYCoordinate(){
		return foundedYCoordinate;
	}
	
	/**
	 * A method to set the founded x-coordinate in the method move.
	 * 
	 * @param	foundedXCoordinate
	 * 			The founded x-coordinate to set.
	 * @post	The new founded x-coordinate is set to the provided value.
	 * 			| new.getFoundedXCoordinate() == foundedXCoordinate
	 * @throws 	IllegalArgumentException
	 * 			An exception is thrown when the coordinate is not valid.
	 * 			| ! isValidCoordinate(foundedXCoordinate)
	 */
	public void setFoundedXCoordinate(double foundedXCoordinate) throws IllegalArgumentException{
		if(!isValidCoordinate(foundedXCoordinate))
			throw new IllegalArgumentException();
		this.foundedXCoordinate = foundedXCoordinate;
	}
	
	/**
	 * A method to set the founded y-coordinate in the method move.
	 * 
	 * @param	foundedYCoordinate
	 * 			The founded y-coordinate to set.
	 * @post	The new founded y-coordinate is set to the provided value.
	 * 			| new.getFoundedYCoordinate() == foundedYCoordinate
	 * @throws 	IllegalArgumentException
	 * 			An exception is thrown when the coordinate is not valid.
	 * 			| ! isValidCoordinate(foundedYCoordinate)
	 */
	public void setFoundedYCoordinate(double foundedYCoordinate) throws IllegalArgumentException{
		if(!isValidCoordinate(foundedYCoordinate))
			throw new IllegalArgumentException();
		this.foundedYCoordinate = foundedYCoordinate;
	}
	
	/**
	 * A method to check whether this worm can move.
	 * 
	 * @post	If the result is true and only if it is true,
	 * 			the founded x-coordinate is set to the founded
	 * 			location.
	 * 			| new.getFoundedXCoordinate
	 *  @post	If the result is true and only if it is true,
	 * 			the founded y-coordinate is set to the founded
	 * 			location.
	 * 			| new.getFoundedYCoordinate
	 * @return	The result is true if the founded location is 
	 * 			passable or adjacent to impassable terrain and
	 * 			if the worm has enough action points to move,
	 * 			otherwise false.
	 * 			| for each distance in 0.1 .. getRadius():
	 * 			|	for each divergence in 0 .. 0.7875:
	 * 			|		if( (getWorld().isPassable(
	 *			|				( (Math.cos(getDirection()+divergence)*distance) + getCoordinateX() ), 
	 *			|				( (Math.sin(getDirection()+divergence)*distance) + getCoordinateY() ), getRadius())) 
	 *			|	 		||	getWorld().isAdjacentToImpassableTerrain(
	 *			|	    			( (Math.cos(getDirection()+divergence)*distance) + getCoordinateX() ),
	 *			|	    			( (Math.sin(getDirection()+divergence)*distance) + getCoordinateY() ), getRadius()))
	 *			|			then if( hasEnoughActionPoints(
	 *			|								 Math.atan(( ((Math.sin(getDirection()+divergence)*distance) + getCoordinateY()) - this.getCoordinateY()) / 
	 *			|										   ( ((Math.cos(getDirection()+divergence)*distance) + getCoordinateX()) - this.getCoordinateX()))) )
	 *			|					then setFoundedXCoordinate( (Math.cos(getDirection()+divergence)*distance) + getCoordinateX() )
	 *			|					&&	 setFoundedYCoordinate( (Math.sin(getDirection()+divergence)*distance) + getCoordinateY() )
	 *			|					&&	 result == true
	 *			|		if( (getWorld().isPassable(
	 *			|				( (Math.cos(Math.abs(getDirection()-divergence))*distance) + getCoordinateX() ), 
	 *			|				( (Math.sin(Math.abs(getDirection()-divergence))*distance) + getCoordinateY() ), getRadius())) 
	 *			|	 		||	getWorld().isAdjacentToImpassableTerrain(
	 *			|	    			( (Math.cos(Math.abs(getDirection()-divergence))*distance) + getCoordinateX() ),
	 *			|	    			( (Math.sin(Math.abs(getDirection()-divergence))*distance) + getCoordinateY() ), getRadius())) 
	 *			|			then if( hasEnoughActionPoints(
	 *			|								 Math.atan(( ((Math.sin(Math.abs(getDirection()-divergence))*distance) + getCoordinateY()) - this.getCoordinateY()) / 
	 *			|								 		   ( ((Math.cos(Math.abs(getDirection()-divergence))*distance) + getCoordinateX()) - this.getCoordinateX()))))
	 *			|					then setFoundedXCoordinate( (Math.cos(Math.abs(getDirection()-divergence))*distance) + getCoordinateX() )
	 *			|					&& 	 setFoundedYCoordinate( (Math.sin(Math.abs(getDirection()-divergence))*distance) + getCoordinateY() )
	 *			|					&&   result == true
	 *			| result == false
	 */	
	public boolean canMove(){
	    for(double distance = getRadius(); distance >= 0.1; distance -= 0.001){
	    	for(double divergence = 0; divergence <= 0.7875; divergence += 0.0175){
				if( (getWorld().isPassable(
						( (Math.cos(getDirection()+divergence)*distance) + getCoordinateX() ), 
						( (Math.sin(getDirection()+divergence)*distance) + getCoordinateY() ), getRadius())) ||
					 getWorld().isAdjacentToImpassableTerrain(
					    ( (Math.cos(getDirection()+divergence)*distance) + getCoordinateX() ),
					    ( (Math.sin(getDirection()+divergence)*distance) + getCoordinateY() ), getRadius())) {
					double tempSlope = Math.atan(( ((Math.sin(getDirection()+divergence)*distance) + getCoordinateY()) - this.getCoordinateY()) / 
												 ( ((Math.cos(getDirection()+divergence)*distance) + getCoordinateX()) - this.getCoordinateX()));
					if(hasEnoughActionPoints(tempSlope)){
						setFoundedXCoordinate( (Math.cos(getDirection()+divergence)*distance) + getCoordinateX() );
						setFoundedYCoordinate( (Math.sin(getDirection()+divergence)*distance) + getCoordinateY() );
						return true;
					}
				}
				if( (getWorld().isPassable(
						( (Math.cos(Math.abs(getDirection()-divergence))*distance) + getCoordinateX() ), 
						( (Math.sin(Math.abs(getDirection()-divergence))*distance) + getCoordinateY() ), getRadius())) ||
					 getWorld().isAdjacentToImpassableTerrain(
					    ( (Math.cos(Math.abs(getDirection()-divergence))*distance) + getCoordinateX() ),
					    ( (Math.sin(Math.abs(getDirection()-divergence))*distance) + getCoordinateY() ), getRadius())) {
					double tempSlope = Math.atan(( ((Math.sin(Math.abs(getDirection()-divergence))*distance) + getCoordinateY()) - this.getCoordinateY()) / 
												 ( ((Math.cos(Math.abs(getDirection()-divergence))*distance) + getCoordinateX()) - this.getCoordinateX()));
					if(hasEnoughActionPoints(tempSlope)){
						setFoundedXCoordinate( (Math.cos(Math.abs(getDirection()-divergence))*distance) + getCoordinateX() );
						setFoundedYCoordinate( (Math.sin(Math.abs(getDirection()-divergence))*distance) + getCoordinateY() );
						return true;
					}
				}
			}
		}
		return false;
	}
	
	/**
	 *  A method to check whether the worm has enough action points to move.
	 *  
	 * @param 	slope
	 * 			The slope to check.
	 * @return	The result is true if the worm has enough action points to move.
	 * 			| result == ( (this.getCurrentActionPoints() - (Math.abs(Math.cos(slope)) + (Math.abs(4*Math.sin(slope)))))
	 * 			|			>= getMinimumActionPoints() )
	 * 			|			
	 */
	public boolean hasEnoughActionPoints(double slope) {
		return (this.getCurrentActionPoints() - ( Math.abs( Math.cos(slope) ) + ( Math.abs( 4*Math.sin(slope) ) ) ) ) >= getMinimumActionPoints();
	}
	
	/**
	 *  A method to move the worm.
	 *  
	 * @effect 	If the worm can move the x-coordinate is set to the founded x-coordinate
	 * 			|  setCoordinateX(getFoundedXCoordinate())
	 * @effect 	If the worm can move the y-coordinate is set to the founded y-coordinate
	 * 			|  setCoordinateY(getFoundedYCoordinate())
	 * @post	If the worm can move the action points are decreased
	 * 			| new.getCurrentActionPoints == (int)Math.ceil( this.getCurrentActionPoints() 
	 * 			|		-	( Math.abs( Math.cos(slope) ) + ( Math.abs( 4*Math.sin(slope)))))
	 * @throws	IllegalArgumentException
	 * 			An exception is thrown when the worm cannot move.
	 * 			| ! canMove()
	 */
	public void move() throws IllegalArgumentException {
		if(!canMove())
			throw new IllegalArgumentException();
		double slope = Math.atan((this.getCoordinateY()-getFoundedYCoordinate())/(this.getCoordinateX()-getFoundedXCoordinate()));
		setCoordinateX(getFoundedXCoordinate());
		setCoordinateY(getFoundedYCoordinate());		
		setCurrentActionPoints( (int)Math.ceil( getCurrentActionPoints() - ( Math.abs( Math.cos(slope) ) + ( Math.abs( 4*Math.sin(slope) ) ) ) ) );
	}
	
	/**
	 * Variable registering the current founded x-coordinate of this worm.
	 */
	private double foundedXCoordinate;
	
	/**
	 * Variable registering the current founded y-coordinate of this worm.
	 */
	private double foundedYCoordinate;
	
	// FALL
	
	/**
	 * A method to check whether this worm can fall.
	 * 
	 * @return the result is true if this worm is on passable terrain but not
	 * 		   Adjacent to impassable terrain.
	 * 		   | result == this.getWorld().isPassable(getCoordinateX(), getCoordinateY(), getRadius())
	 * 		   |	&& 	!this.getWorld().isAdjacentToImpassableTerrain(getCoordinateX(), getCoordinateY(), getRadius())
	 */	
	public boolean canFall() {
		return this.getWorld().isPassable(getCoordinateX(), getCoordinateY(), getRadius()) 
				&& !this.getWorld().isAdjacentToImpassableTerrain(getCoordinateX(), getCoordinateY(), getRadius());
	}
	
	/**
	 * A method to let this worm fall.
	 * 
	 * @post	If this worm can fall and this worm is not terminated
	 * 			then this worm will fall until it finds a location that
	 * 			is adjacent to impassable terrain.
	 * 			| while ( this.getWorld().isPassable(getCoordinateX(), getCoordinateY(), getRadius()) && 
	 *			|		 !this.getWorld().isAdjacentToImpassableTerrain(getCoordinateX(), getCoordinateY(), getRadius()) && 
	 *			|			 !this.isTerminated())
	 *			|		setCoordinateY(getCoordinateY()-0.001)
	 *			|		if(this.getCoordinatY < 0)
	 *			|			then this.terminate()
	 * @post	If this worm has fallen, his hit points are decreased.
	 *			| setCurrentHitPoints(getCurrentHitPoints()-3*(int)Math.floor(oldCoordinateY-getCoordinateY()))
	 * @throws 	IllegalArgumentException
	 * 			An exception is thrown when the worm cannot fall.
	 * 			| ! canFall()
	 */
	public void fall() throws IllegalArgumentException {
		if(!canFall())
			throw new IllegalArgumentException();
		double oldCoordinateY = getCoordinateY();
		while( this.getWorld().isPassable(getCoordinateX(), getCoordinateY(), getRadius()) && 
				!this.getWorld().isAdjacentToImpassableTerrain(getCoordinateX(), getCoordinateY(), getRadius()) && 
				 !this.isTerminated()){
			setCoordinateY(getCoordinateY()-0.01);
			if(this.getCoordinateY() < 0){
				this.terminate();
			}
		}
		int metersOfFall = (int)Math.floor(oldCoordinateY-getCoordinateY());
		setCurrentHitPoints(getCurrentHitPoints()-3*metersOfFall);
	}
	
	// TURN
	
	/**
	 * A method that changes the direction of this worm with a given value.
	 * 
	 * @param 		direction
	 * 				The new direction of this worm.
	 * @pre			The provided value has to be a valid direction.
	 * 				|isValidDirection(direction)
	 * @pre			The worm must have enough action points.
	 * 				|canTurn(direction)
	 * @post		The new direction is equal to the current direction plus the provided value.
	 * 				| new.getDirection() == this.getDirection() + direction
	 * @post		The action points needed for the turn must be subtracted from the current action points.
	 * 				| new.getCurrentActionPoints() == getCurrentActionPoints() - Math.abs((60/((2*Math.PI)/direction))) 
	 */
	public void turn(double direction) {
		assert isValidDirection(direction);
		assert canTurn(direction);
		setDirection(this.getDirection()+direction);
		setCurrentActionPoints((int)Math.ceil(this.getCurrentActionPoints() - Math.abs((60/((2*Math.PI)/direction)))));
	}
	
	/**
	 * A method to check whether this worm has enough action points to turn.
	 * 
	 * @return 		returns true when the used amount of action point, 
	 * 				subtracted from the current amount of action point is bigger than the minimum amount of action points.
	 * 				| result == Math.ceil(this.getCurrentActionPoints() - (direction / Math.abs((60/((2*Math.PI)/direction))))) >= this.getMinimumActionPoints()
	 */
	public boolean canTurn(double direction) {
		return (Math.ceil(this.getCurrentActionPoints() - Math.abs(60/((2*Math.PI)/direction))) >= this.getMinimumActionPoints());
	}
	
	//JUMP 
	
	/**
	 * A method that returns the force invoked on this worm.
	 * 
	 * @return		The force invoked on this worm.
	 * 				| result == (5*getCurrentActionPoints())+(getMass()*getAccelerationOfEarth())
	 */
	public double getForce(){
		return ((5*getCurrentActionPoints())+(getMass()*getAccelerationOfEarth()));
	}
	
	/**
	 * A method that returns the acceleration of the earth.
	 */
	@Immutable
	public double getAccelerationOfEarth(){
		return 9.80665;
	}
	
	/**
	 * A method that returns the velocity of this worm.
	 * 
	 * @return		The velocity of this worm.
	 * 				| result == (getForce()/getMass())*0.5
	 */
	public double getVelocity(){
		return (((getForce()/getMass()))*0.5);
	}
	
	/**
	 *  A method to check whether the x-coordinate and y-coordinate 
	 *  are valid in the world of this worm.
	 *  
	 * @param	xCoordinate
	 * 			The x-coordinate to check.
	 * @param	yCoordinate
	 * 			The y-coordinate to check.
	 * @return	result is true when the coordinates are in the boundaries
	 * 			 of the world of this worm.
	 * 			| result == (xCoordinate >= 0) 
	 *			|      		&& (xCoordinate <= this.getWorld().getWidth()) 
	 *			|			&& (yCoordinate >= 0) 
	 *			|			&& (yCoordinate <= this.getWorld().getHeight())
	 */
	public boolean validWorldCoordinates(double xCoordinate, double yCoordinate){
		return (xCoordinate >= 0) 
				&& (xCoordinate <= this.getWorld().getWidth()) 
				&& (yCoordinate >= 0) 
				&& (yCoordinate <= this.getWorld().getHeight());
	}
	
	/**
	 * A method to check whether this worm can jump.
	 * 
	 * @return 		The method returns true if the current action points are bigger than
	 * 				the minimum amount of action points and if the worm is located on passable terrain.
	 * 				| result == (this.getCurrentActionPoints() > this.getMinimumActionPoints()) 
	 * 				| && ( this.getWorld().isPassable(getCoordinateX(), getCoordinateY(), getRadius())))
	 */
	public boolean canJump(){
		return ((this.getCurrentActionPoints() > this.getMinimumActionPoints()) 
				&& ( this.getWorld().isPassable(getCoordinateX(), getCoordinateY(), getRadius())));
	}
	
	/**
	 * A method to let this worm jump
	 * 
	 * @param 	timeStep
	 * 			The timeStep An elementary time interval during which you may
	 * 			assume that the projectile will not completely move through a 
	 * 			piece of impassable terrain.
	 * @effect	The x-coordinate is set to the founded location.
	 * 			| setCoordinateX(this.jumpStep(getJumpTime(timeStep))[0])
	 * @effect	The y-coordinate is set to the founded location.
	 * 			| setCoordinateY(this.JumpStep(getJumpTime(timeStep))[1])
	 * @throws 	IllegalArgumentException
	 * 			An exception is thrown when this worm cannot jump.
	 * 			| ! this.canJump()
	 */
	public void jump(double timeStep) throws IllegalArgumentException{
		if(!this.canJump())
			throw new IllegalArgumentException();
		double[] inFlightCoordinates = this.jumpStep(getJumpTime(timeStep));
		this.setCoordinateX(inFlightCoordinates[0]);
		this.setCoordinateY(inFlightCoordinates[1]);
		this.setCurrentActionPoints(0);
	}
	
	/**
	 * A method to determine the time that this worm needs until it hits passable terrain
	 * adjacent to impassable terrain or leaves the world.
	 * 
	 * @param	timeStep
	 * 			timeStep An elementary time interval during which you may assume
	 *          that the worm will not completely move through a piece of impassable terrain.
	 * @return	The time needed for this worm until it hits passable terrain adjacent to impassable
	 * 			terrain or leaves the world.
	 * 			| double time = 0
	 * 			| while( (this.getWorld().isPassable(inFlightCoordinates[0], inFlightCoordinates[1], getRadius()))
	 *			|	    && (this.getWorld().isAdjacentToImpassableTerrain(inFlightCoordinates[0], inFlightCoordinates[1], getRadius()))
	 *			|		&&	validWorldCoordinates(inFlightCoordinates[0], inFlightCoordinates[1]) )
	 *			|		time = time + timeStep
	 *			|		inFlightCoordinates = this.jumpStep(time)
	 *			| while( (this.getWorld().isPassable(inFlightCoordinates[0], inFlightCoordinates[1], getRadius())) 
	 *			|		&& !(this.getWorld().isAdjacentToImpassableTerrain(inFlightCoordinates[0], inFlightCoordinates[1], getRadius()))
	 *			|		&&	validWorldCoordinates(inFlightCoordinates[0], inFlightCoordinates[1]) )
	 *			|		time = time + timeStep
	 *			|		inFlightCoordinates = this.jumpStep(time);
	 *			| return time
	 */
	public double getJumpTime(double timeStep) {
		double time = 0;
		double[] inFlightCoordinates = this.jumpStep(time);
		while( (this.getWorld().isPassable(inFlightCoordinates[0], inFlightCoordinates[1], getRadius()))
				&& (this.getWorld().isAdjacentToImpassableTerrain(inFlightCoordinates[0], inFlightCoordinates[1], getRadius()))
				&& 	validWorldCoordinates(inFlightCoordinates[0], inFlightCoordinates[1])){
			time = time + timeStep;
			inFlightCoordinates = this.jumpStep(time);
		}
		while( (this.getWorld().isPassable(inFlightCoordinates[0], inFlightCoordinates[1], getRadius())) 
				&& !(this.getWorld().isAdjacentToImpassableTerrain(inFlightCoordinates[0], inFlightCoordinates[1], getRadius()))
				&& 	validWorldCoordinates(inFlightCoordinates[0], inFlightCoordinates[1])){
			time = time + timeStep;
			inFlightCoordinates = this.jumpStep(time);
		}
		return time;
	}
	
	/**
	 * A method to compute the in-flight positions x-coordinate and 
	 * y-coordinate at any time after the worm is launched.
	 * 
	 * @param 	time
	 * 			The time after the worm is launched.
	 * @return	An array containing the new values for the X and Y coordinate at time.
	 *			|result == 
	 *			| new.getCoordinateX = this.getCoordinateX() + (((this.getVelocity()*Math.cos(this.getDirection()))*time))
	 *			| new.getCoordinateY = this.getCoordinateY() + ((this.getVelocity()*Math.sin(this.getDirection())*time) - ((this.getAccelerationOfEarth()*Math.pow(time, 2))/2))
	 *			| double[] result = {new.getCoordinateX, new.getCoordinateY}
	 * @throws 	IllegalArgumentException
	 * 			The method throws the exception when the worm cannot be launched.
	 * 			| ! this.canJump()
	 */
	public double[] jumpStep(double time) throws IllegalArgumentException{
		if(!this.canJump())
			throw new IllegalArgumentException();
		double x = this.getCoordinateX() + (((this.getVelocity()*Math.cos(this.getDirection()))*time));
		double y = this.getCoordinateY() + ((this.getVelocity()*Math.sin(this.getDirection())*time) - ((this.getAccelerationOfEarth()*Math.pow(time, 2))/2));
		double[] result = {x, y};
		return result;
	}

	// WORLD
	
	/** 
	 * Return the world of this worm.
	 */
	@Basic @Raw 
	public World getWorld() {
		return this.world;
	}
	
	/**
	 * Check whether this worm can have the given world as its world.
	 * 
	 * @param	world
	 *          The world to check.
	 * @return	If this worm is not yet terminated, true if and
	 *          only if the given world is effective.
	 *        	| if (! isTerminated())
	 *       	|   then result == (world != null) 
	 * @return	If this worm is terminated, true if and only if
	 *          the given world is not effective.
	 *        	| if (! this.isTerminated())
	 *       	|   then result == (world == null)
	 */
	@Raw
	public boolean canHaveAsWorld(World world) {
		if (isTerminated())
			return (world == null);
		return (world != null);
	}
	
	/**
	 * Check whether this worm has a proper world.
	 * 
	 * @return	True if and only if this worm can have its world as its
	 * 			world, and if this worm is terminated or the world of
	 *          this worm has this worm as one of its worms.
	 *       	| result ==
	 *        	|   canHaveAsWorld(getWorld()) &&
	 *       	|   ( isTerminated() || getWorld().hasAsWorm(this))
	 */
	public boolean hasProperWorld() {
		return canHaveAsWorld(getWorld()) &&
				 ( isTerminated() || getWorld().hasAsWorm(this));
	}
		
	/** 
	 * Register the given world as the world of this worm.
	 * 
	 * @param	world
	 *          The world to be registered as the world of this worm.
	 * @post   	The world of this worm is the same as the given world.
	 *       	| new.getWorld() == world
	 * @throws	IllegalArgumentException
	 * 			An exception is thrown when the provided world is not valid.
	 * 			| ! canHaveAsWorld(world)
	 */
	@Raw
	private void setWorld(World world) throws IllegalArgumentException {
		if( !canHaveAsWorld(world) )
			throw new IllegalArgumentException("The provided world is not a valid world.");
		this.world = world;
	}
		
	/**
	 * Variable referencing the world of this worm.
	 */
	private World world = null;
	
	// TERMINATE
		
	/**
	 * Check whether this worm is terminated.
	 */
	@Basic @Raw
	public boolean isTerminated() {
		return this.getState() == State.TERMINATED;
	}
		
	/**
	 * Terminate this worm.
	 * 
	 * @post	This worm is terminated.
	 *       	| new.isTerminated()
	 * @post   	If this worm was not yet terminated, this worm
	 *        	is no longer one of the worms of the world to which
	 *        	this worm belonged.
	 *     		| if (! isTerminated())
	 *    	    |   then ! (new getWorld()).hasAsWorm(this))
	 * @post  	If this worm was not yet terminated, the number of
	 *      	worms of the world to which this worm belonged is
	 *        	decremented by 1.
	 *     		| if (! isTerminated())
	 *      	|   then (new getWorld()).getWorms().size() ==
	 *      	|            this.getWorld().getWorms().size() - 1
	 * @post  	If this worm was not yet terminated, all worms
	 *        	of the world to which this worm belonged registered at an
	 *        	index beyond the index at which this worm was registered,
	 *        	are shifted one position to the left.
	 *       	| for each I,J in 0..getWorld().getWorms().size()-1:
	 *       	|   if ( (getWorld().getWorms().get(I) == worm) and (I < J) )
	 *       	|     then (new getWorld()).getWorms().get(J-1) == getWorld().getWorms().get(J)
	 * @post   	If this worm was not yet terminated, this worm
	 *        	is no longer one of the worms of the team to which
	 *        	this worm belonged.
	 *     		| if (! isTerminated())
	 *    	    |   then ! (new getTeam()).hasAsWorm(this))
	 * @post  	If this worm was not yet terminated, the number of
	 *      	worms of the team to which this worm belonged is
	 *        	decremented by 1.
	 *     		| if (! isTerminated())
	 *      	|   then (new getTeam()).getWorms().size() ==
	 *      	|            this.getTeam().getWorms().size() - 1
	 * @post  	If this worm was not yet terminated, all worms
	 *        	of the team to which this worm belonged registered at an
	 *        	index beyond the index at which this worm was registered,
	 *        	are shifted one position to the left.
	 *       	| for each I,J in 0..getTeam().getWorms().size()-1:
	 *       	|   if ( (getTeam().getWorms().get(I) == worm) and (I < J) )
	 *       	|     then (new getTeam()).getWorms().get(J-1) == getTeam().getWorms().get(J)
	 */
	public void terminate() {
		if (!isTerminated()) {
			setState(State.TERMINATED);
			World oldWorld = getWorld();
			this.setWorld(null);
			oldWorld.removeWorm(this);
			Team oldTeam = getTeam();
			this.setTeam(null);
			oldTeam.removeWorm(this);
		}
	}
	
	// STATE
	
	/**
	 * Enumeration of all possible states of this worm.
	 */
	private static enum State {
		ALIVE, TERMINATED;
	}
		
	/**
	 * Return the state of this worm.
	 */
	@Raw
	private State getState() {
		return this.state;
	}

	/**
	 * Set the state of this worm to the given state.
	 * 
	 * @param	state
	 *        	The new state for this worm.
	 * @pre		The given state must exists.
	 *       	| state != null
	 * @post	The state of this worm is the same as the given state.
	 *     		| new.getState() == state
	 */
	private void setState(State state) {
		assert (state != null);
		this.state = state;
	}

	/**
	 * Variable registering the state of this worm.
	 */
	private State state = State.ALIVE;

	// LIFE
		
	/**
	 *  A method to return whether the given worm is alive.
	 *  
	 * @effect	If the result is false and only if it is false,
	 * 			This worm is terminated.
	 * 			| new.isTerminated()
	 * @return	The result is true if the worm his hit points are greater then zero.
	 * 			| result == getCurrentHitPoints() > 0
	 */
	public boolean isAlive() {
		if(getCurrentHitPoints() <= 0)
			this.terminate();
		return isTerminated();
	}
		
	// SHOOTING
	
	/**
	 * A method to check whether the propulsion yield is valid.
	 * 
	 * @param	propulsionYield
	 * 			The propulsion yield to check.
	 * @return	the result is true if the given propulsion yield is between
	 * 			0 and 100
	 * 			| result == (propulsionYield >= 0) && (propulsionYield <= 100)
	 */
	public boolean isValidPropulsionYield(int propulsionYield){
		return (propulsionYield >= 0) && (propulsionYield <= 100);
	}
	
	/**
	 * A method to shoot with this worm.
	 * 
	 * @param	propulsionYield
	 * 			The given propulsion yield.
	 * @pre		The propulsionYield must be valid.
	 * 			| isValidPropulsionYield(propulsionYield)
	 * @post	If the selected weapon is the Rifle, then
	 * 			we created a rifleBullet and decreased the current	
	 * 			action points of the worm with the shooting cost of
	 * 			the rifleBullet.	
	 * 			| if( this.getSelectedWeapon() instanceof Rifle )
	 * 			|  		then	double x = (this.getCoordinateX()+(Math.cos(this.getDirection())*this.getRadius())+0.1)
	 *			|			&&	double y = (this.getCoordinateY()+(Math.sin(this.getDirection())*this.getRadius())+0.1)
	 * 			|			&&	RifleBullet rifleBullet = new RifleBullet(x, y, this.getDirection(), this.getSelectedWeapon(), this.getWorld())
	 * 			|			&& setCurrentActionPoints(getCurrentActionPoints()-rifleBullet.getShootingCost())
	 * @post	If the selected weapon is the Bazooka, then
	 * 			We created a BazookaBullet and decreased the current
	 * 			action points of the worm with the shooting cost of
	 * 			the bazookaBullet.
	 * 			| if ( this.getSelectedWeapon() instanceof Bazooka )
	 *			|  		then	double x = (this.getCoordinateX()+(Math.cos(this.getDirection())*this.getRadius())+0.1)
	 *			|			&&	double y = (this.getCoordinateY()+(Math.sin(this.getDirection())*this.getRadius())+0.1)
	 *			|			&&	BazookaBullet bazookaBullet = new BazookaBullet(x, y, this.getDirection(), this.getSelectedWeapon(), this.getWorld(), propulsionYield)
	 *			|			&&	setCurrentActionPoints(getCurrentActionPoints()-bazookaBullet.getShootingCost())
	 */
	public void shoot(int propulsionYield){
		assert isValidPropulsionYield(propulsionYield);
		if (this.getSelectedWeapon() instanceof Rifle){
			double x = (this.getCoordinateX()+(Math.cos(this.getDirection())*this.getRadius())+0.1);
			double y = (this.getCoordinateY()+(Math.sin(this.getDirection())*this.getRadius())+0.1);
			RifleBullet rifleBullet = new RifleBullet(x, y, this.getDirection(), this.getSelectedWeapon(), this.getWorld());
			setCurrentActionPoints(getCurrentActionPoints()-rifleBullet.getShootingCost());
		}
		if (this.getSelectedWeapon() instanceof Bazooka){
			double x = (this.getCoordinateX()+(Math.cos(this.getDirection())*this.getRadius())+0.1);
			double y = (this.getCoordinateY()+(Math.sin(this.getDirection())*this.getRadius())+0.1);
			BazookaBullet bazookaBullet = new BazookaBullet(x, y, this.getDirection(), this.getSelectedWeapon(), this.getWorld(), propulsionYield);
			setCurrentActionPoints(getCurrentActionPoints()-bazookaBullet.getShootingCost());
		}
	}
	
	// TEAM
	
	/** 
	 * Return the team of this worm.
	 *    A null reference is returned if this worm has no team.
	 */
	@Basic @Raw
	public Team getTeam() {
		return this.team;
	}
	
	/**
	 * Check whether this worm can have the given team as his team.
	 *
	 * @param	team
	 *         	The team to check.
	 * @return 	True if the team is not effective.
	 *       	| if (team == null)
	 *       	|   then result == true
	 * @return	if the team is effective the team may not 
	 * 			contain this worm.
	 * 			|if (team != null)
	 * 			| 	then result == !(team.hasAsWorm(this))
	 */
	@Raw
	public boolean canHaveAsTeam(@Raw Team team) {
		if (team == null)
			return true;
		else
			return !(team.hasAsWorm(this));
	}
	
	/**
	 * Check whether this worm has a proper team.
	 *
	 * @return	True if and only if this worm can have its team
	 *         	as its team, and if the team is effective, and has this
	 *         	worm as one of its worms.
	 *       	| result ==
	 *       	|   canHaveAsTeam(getTeam()) &&
	 *       	|   ( (getTeam() == null) ||
	 *       	|     (getTeam().hasAsWorm(this))
	 */
	@Raw
	public boolean hasProperTeam() {
		return canHaveAsTeam(this.getTeam()) 
				&& ( (this.getTeam() == null) || (this.getTeam().hasAsWorm(this)) );
	}
	
	/** 
	 * Register the given team as the team of this worm.
	 * 
	 * @param	team
	 *         	The team to be registered as the team of this worm.
	 * @pre    	This team must be able to have the given worm as one
	 *          of its worms.
	 *       	| canHaveAsTeam(team)
	 * @post   	The team of this worm is the same as the given team.
	 *       	| new.getTeam() == team
	 */
	@Raw
	private void setTeam(@Raw Team team) {
		assert canHaveAsTeam(team);
		this.team = team;
	}
	
	/**
	 * Variable referencing the team of this worm.
	 */
	private Team team;
	
	// WEAPONS
	
	/**
	 * A method that returns a list of all the weapons of this worm.
	 */
	@Basic
	public ArrayList<Weapon> getWeapons() {
		return weaponObjects;
	}
	
	/**
	 * Check whether this worm can have the given weapon as one 
	 * of its weapons.
	 * 
	 * @param	weapon
	 *        	The weapon to check.
	 * @return	True if and only if the given weapon is effective
	 *        	and already references this worm, and this worm
	 *        	does not yet have a weapon of the class of the given weapon.
	 *       	| result ==
	 *       	|   (weapon != null) && (weapon.getWorm() == this)
				|		&& (!this.hasAsWeapon(weapon))
	 */
	@Raw
	public boolean canHaveAsWeapon(Weapon weapon) {
		return (weapon != null) && (weapon.getWorm() == this)
				&& (!this.hasAsWeapon(weapon));
	}
	
	/**
	 * Check whether this worm has a weapon of the class of the given weapon.
	 * 
	 * @param  	weapon
	 * 		   	The weapon to check.
	 * @return	The result is true when the worm has a weapon of the class 
	 * 			of the provided weapon, false otherwise.
	 *       	| for each I in 0..getWeapons().size()-1:
	 *      	|   if ( getWeapons().get(I).getClass() == weapon.getClass() ) 
	 *      	|     then result == true
	 *      	| result == false
	 */
	public boolean hasAsWeapon(@Raw Weapon weapon) {
		for(int i = 0; i < getWeapons().size(); i++)
			if(getWeapons().get(i).getClass() == weapon.getClass())
				return true;
		return false;
	}
	
	/**
	 * Add the given weapon to the list of weapons of this worm.
	 * 
	 * @param	weapon
	 *         	The weapon to be added.
	 * @post	The number of weapons of this worm is
	 *        	incremented by 1.
	 *      	| new.getWeapons().size() == this.getWeapons().size() + 1
	 * @post   	This worm has the given weapon as its very last weapon.
	 *       	| new.getWeapons().get(getWeapons().size()+1) == weapon
	 * @throws	IllegalArgumentException
	 * 			An exception is thrown when the provided weapon is not a valid weapon.
	 * 			| ! canHaveAsWeapon(weapon)
	 */
	@Basic
	public void addWeapon(Weapon weapon) throws IllegalArgumentException{
		if(!canHaveAsWeapon(weapon))
			throw new IllegalArgumentException("The provided weapon is not a valid weapon.");
		weaponObjects.add(weapon);
	}
	
	/**
	 * A method to return the current selected weapon.
	 */
	public Weapon getSelectedWeapon(){
		return selectedWeapon;
	}
	
	/**
	 * A method to set the weapon of this worm.
	 * 
	 * @param	weapon
	 * 			The new weapon for this worm.
	 * @post	The new weapon for this worm is set to the given weapon.
	 * 			| new.getSelectedWeapon == weapon
	 */
	public void setSelectedWeapon(Weapon weapon){
		selectedWeapon = weapon;
	}
		
	/**
	 * A method to select the next weapon.
	 * 
	 * @post	If this worm has weapons and if the current selected weapon
	 * 			is the last weapon of this worm then is the new selected weapon
	 * 			the first weapon of this worm.
	 * 			| if( getWeapons().size() != 0 )
	 * 			|	then if( getWeapons().indexOf(getSelectedWeapon())+1 == getWeapons().size() )
	 * 			|			then setSelectedWeapon(getWeapons().get(0))
	 * @post	If this worm has weapons and if the current selected weapon
	 * 			is not the last weapon of this worm then the new selected weapon
	 * 			is the next weapon of this worm.
	 * 			| if( getWeapons().size() != 0 )
	 * 			| 	then if( getWeapons().indexOf(getSelectedWeapon())+1 != getWeapons().size() )
	 * 			|		then setSelectedWeapon(getWeapons().get( getWeapons().indexOf(getSelectedWeapon()) + 1 ))
	 */
	public void selectNextWeapon() {
		if(getWeapons().size() != 0){
			if(getWeapons().indexOf(getSelectedWeapon())+1 == getWeapons().size())
				setSelectedWeapon(getWeapons().get(0));
			else{
				setSelectedWeapon(getWeapons().get( getWeapons().indexOf(getSelectedWeapon()) + 1 ));
			}
		}
	}
	
	/**
	 * Variable referencing the current selected weapon of this worm.
	 */
	Weapon selectedWeapon;
	
	/**
	 * Variable referencing a list collecting all the weapons of this worm.
	 */
	ArrayList<Weapon> weaponObjects = new ArrayList<Weapon>();

}
