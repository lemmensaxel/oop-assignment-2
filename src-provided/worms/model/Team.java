package worms.model;

import java.util.ArrayList;

import be.kuleuven.cs.som.annotate.Basic;
import be.kuleuven.cs.som.annotate.Raw;

/**
 * A class representing the teams of a world. 
 * 
 * @invar	Each team must have a valid name.
 *       	| isValidName(getName())
 * @invar  	Each team must have a proper world.
 *       	| hasProperWorld()
 *       
 * @version 2.0
 * @author  Axel Lemmens & Maarten Rimaux
 */
public class Team {
	
	// CONSTRUCTOR
	
	/**
	 * @param	nameOfTeam
	 * 			The provided name for the new team.
	 * @param	world
	 * 			The provided world for the new team.
	 * @effect 	The name for this new team is set to the given name.
	 *      	| setName(nameOfTeam)
	 * @post	The new world of this new team is equal to the provided "world"
	 * 			| new.getWorld() == world
	 * @post   	The number of teams for the given world is
	 *         	incremented by 1.
	 *       	| (new world).getTeams().size() == world.getTeams().size() + 1
	 * @post   	The given world has this new team as its very last team.
	 *      	| (new world).getTeams().get(world.getTeams().size()+1) == this
	 * @throws	IllegalArgumentException
	 * 			An exception is thrown when the provided string is not a valid name.
	 * 			| ! isValidName(name)
	 * @throws	IllegalArgumentException
	 * 			An exception is thrown when this new team cannot have the given world as
	 *         	its world.
	 * 			| ! canHaveAsWorld(world)
	 */
	public Team(String nameOfTeam, World world) throws IllegalArgumentException {
		setName(nameOfTeam);
		setWorld(world);
		world.addTeam(this);
	}
	
	// NAME
	
	/**
	 * A method to get the name of this team.
	 */
	@Basic
	public String getName() {
		return name;
	}
	
	/**
	 * A method to check whether a given string for name is valid.
	 * 
	 * @param 		name
	 * 				The name to check.
	 * @return		The result should be a string starting with an uppercase letter, 
	 * 				at least two characters long and contains only letters. 
	 * 				| result ==  (string.matches("^[A-Z][a-zA-Z]{1,}")
	 */
	public static boolean isValidName(String name){
		return name.matches("^[A-Z][a-zA-Z]{1,}");
	}
	
	/**
	 * A method to set the name of this team to a given string.
	 * 
	 * @param		name
	 * 				The new name of this team.
	 * @post		The name of this team is set to the provided value.
	 * 				| new.getName() == name
	 * @throws		IllegalArgumentException
	 * 				The method throws the exception when the provided string is not valid.
	 * 				| (! isValidName(name))
	 */
	public void setName(String name) throws IllegalArgumentException {
		if ( ! isValidName(name))
			throw new IllegalArgumentException("The provided string is not a valid string.");
		this.name = name;
	}
		
	/**
	 * Variable registering the name of this team.
	 */
	private String name;
	
	//	WORM
	
	/**
	 * A method to return a list of all the worms in this team.
	 */
	@Basic
	public ArrayList<Worm> getWorms() {
		return wormObjects;
	}
	
	/**
	 * Check whether this team can have the given worm
	 * as one of its worms.
	 * 
	 * @param	worm
	 *        	The worm to check.
	 * @return	True if and only if the given worm is effective
	 *        	and already references this team, and this team
	 *        	does not yet have the given worm as one of its worms.
	 *       	| result ==
	 *       	|   (worm != null) && (worm.getTeam() == this)
	 *			|		&& (!this.hasAsWorm(worm))
	 */
	@Raw
	public boolean canHaveAsWorm(Worm worm) {
		return (worm != null) && (worm.getTeam() == this)
				&& (!this.hasAsWorm(worm));
	}
	
	/**
	 * Check whether this team has the given worm as one of its worms.
	 * 
	 * @param  	worm
	 * 		   	The worm to check.
	 * @return	The result is true when the provided worm is a worm of this team.
	 *       	| result == getWorms().contains(worm)
	 */
	public boolean hasAsWorm(@Raw Worm worm) {
		return getWorms().contains(worm);
	}
	
	/**
	 * Add the given worm to the list of worms of this team.
	 * 
	 * @param	worm
	 *         	The worm to be added.
	 * @post	The number of worms of this team is
	 *        	incremented by 1.
	 *      	| new.getWorms().size() == this.getWorms().size() + 1
	 * @post   	This team has the given worm as its very last worm.
	 *       	| new.getWorms().get(getWorms().size()+1) == worm
	 * @throws	IllegalArgumentException
	 * 			An exception is thrown when the provided worm is not a valid worm.
	 * 			| ! canHaveAsWorm(Worm)
	 */
	@Basic
	public void addWorm(Worm worm) throws IllegalArgumentException{
		if(!canHaveAsWorm(worm))
			throw new IllegalArgumentException("The provided worm is not a valid worm.");
		wormObjects.add(worm);
	}
	
	/**
	 * 	A method to remove the given worm from the list of worms of this team.
	 * 
	 * @param	worm
	 *         	The worm to be removed.
	 * @post   	The number of worms of this team is
	 *         	decremented by 1.
	 *       	| new.getWorms().size() == getWorms().size() - 1
	 * @post    This team has no longer the given worm as
	 *      	one of its worms.
	 *       	| ! new.hasAsWorm(worm)
	 * @post   	All worms registered at an index beyond the index at
	 *        	which the given worm was registered, are shifted
	 *       	one position to the left.
	 *       	| for each I,J in 0..getNbWorms()-1:
	 *      	|   if ( (getWorms().get(I) == worm) and (I < J) )
	 *      	|     then new.getWorms().get(J-1) == getWorms().get(J)
	 * @throws	IllegalArgumentException
	 * 			An exception is thrown when the worm doesn't exists or
	 * 			if this team has not the given worm as one of its worms or
	 * 			if the given worm his team still exists.
	 * 			| (worm == null) || !this.hasAsWorm(worm) || !(worm.getTeam() == null)
	 */
	@Raw
	public void removeWorm(Worm worm) throws IllegalArgumentException {
		if( (worm == null) || !this.hasAsWorm(worm) || !(worm.getTeam() == null) )
			throw new IllegalArgumentException("The provided worm is not a valid worm.");
		getWorms().remove(worm);
	}
	
	/**
	 * Variable referencing a list collecting all the worms in this team.
	 */
	private ArrayList<Worm> wormObjects = new ArrayList<Worm>();
	
	// WORLD
	
	/** 
	 * Return the world of this team.
	 */
	@Basic @Raw
	public World getWorld() {
		return this.world;
	}
	
	/**
	 * Check whether this team can have the given world as its world.
	 *
	 * @param	world
	 *			The world to check.
	 * @return	True if the given world is effective and if 
	 * 			the given world has less teams than the maximum number of teams,
	 * 			otherwise false.
	 *      	| if (world != null)
	 *    		|   then if(world.getTeams().size()-1 < world.getMaxNumberOfTeams()) 
	 *       	|			then result == true 
	 *       	|else
	 *       	|	 result == false
	 * 
	 */
	public boolean canHaveAsWorld(World world) {
		if(world != null)
			if(world.getTeams().size()-1 < world.getMaxNumberOfTeams())
				return true;
		return false;
	}
	
	/**
	 * Check whether this team has a proper world.
	 * 
	 * @return  True if and only if this team can have its world as its
	 *          world, and if the world of this team has this team as 
	 *          one of its teams.
	 *        | result ==
	 *        |   canHaveAsWorld(getWorld()) && getWorld().hasAsTeam(this)
	 */
	public boolean hasProperWorld() {
		return canHaveAsWorld(getWorld()) && getWorld().hasAsTeam(this);
	}
	
	/** 
	 * Register the given world as the world of this team.
	 * 
	 * @param	world
	 *          The world to be registered as the world of this team.
	 * @post   	The world of this team is the same as the given world.
	 *       	| new.getWorld() == world
	 * @throws	IllegalArgumentException
	 * 			An exception is thrown when the provided world is not valid.
	 * 			| ! canHaveAsWorld(world)
	 */
	@Raw
	private void setWorld(World world) throws IllegalArgumentException {
		if( !canHaveAsWorld(world) )
			throw new IllegalArgumentException("The provided world is not a valid world");
		this.world = world;
	}

	/**
	 * Variable referencing the world of this team.
	 */
	private World world;

}
