package worms.model;

import be.kuleuven.cs.som.annotate.Basic;
import be.kuleuven.cs.som.annotate.Raw;

/**
 * A class of rifleBullets, a subclass of Projectile.
 *
 * @version  2.0
 * @author   Axel Lemmens & Maarten Rimaux
 */
public class RifleBullet extends Projectile {
	
	public RifleBullet(double xCoordiante, double yCoordinate,
			double direction, Weapon weapon, World world)
			throws IllegalArgumentException {
		super(xCoordiante, yCoordinate, direction, weapon, world);
	}

	/**
	 * A method to return the current radius of this rifleBullet expressed in meters.
	 * 
	 * @return	The radius of this rifleBullet.
	 * 			| return == Math.cbrt( ( getMass()/( getDensity()*(4.0/3.0)*(Math.PI) ) ) )
	 */
	@Override
	@Basic
	public double getRadius() {
		return Math.cbrt( ( getMass()/( getDensity()*(4.0/3.0)*(Math.PI) ) ) );
	}
	
	/**
	 * A method to return the mass of this rifleBullet expressed in kilogram.
	 */
	@Override
	@Basic
	public double getMass() {
		return mass;
	}

	/**
	 * A method to return the force of this rifleBullet expressed in Newton.
	 */
	@Override
	@Basic
	public double getForce() {
		return force;
	}

	/**
	 * A method to return the shooting cost of this rifleBullet.
	 */
	@Override
	@Basic
	public int getShootingCost() {
		return shootingCost;
	}

	/**
	 * A method to return the hit cost of this rifleBullet.
	 */
	@Override
	@Basic
	public int getHitCost() {
		return hitCost;
	}
	
	/**
	 * A method to check whether this rifleBullet can be shot.
	 * 
	 * @return 		The method returns true if the current action points of the worm of the weapon of this rifleBullet
	 * 				are bigger than or equal to the shooting cost of this rifleBullet and if the worm of the weapon of
	 * 				this rifleBullet is located on passable terrain.
	 * 				| result == (this.getWeapon().getWorm().getCurrentActionPoints() >= this.getShootingCost()) 
	 * 				| && (this.getWorld().isPassable(this.getWeapon().getWorm().getCoordinateX(), 
	 * 				|								 this.getWeapon().getWorm().getCoordinateY(), 
	 * 				|								 this.getWeapon().getWorm().getRadius()))
	 */
	@Override
	public boolean canShoot(){
		return (this.getWeapon().getWorm().getCurrentActionPoints() >= this.getShootingCost()) 
 				 && ( this.getWorld().isPassable(this.getWeapon().getWorm().getCoordinateX(), 
 						 						 this.getWeapon().getWorm().getCoordinateY(), 
 						 						 this.getWeapon().getWorm().getRadius()));
	}
	
	/**
	 * Check whether this rifleBullet can have the given weapon as its current weapon.
	 * 
	 * @param	weapon
	 *          The weapon to check.
	 * @return	If this rifleBullet is not yet terminated, true if and
	 *          only if the given weapon is effective and the class of
	 *          the weapon is equal to the rifle class.
	 *        	| if (! isTerminated())
	 *       	|   then result == (weapon != null) && (weapon instanceof Rifle)
	 * @return	If this rifleBullet is terminated, true if and only if
	 *          the given weapon is not effective.
	 *        	| if (this.isTerminated())
	 *       	|   then result == (weapon == null)
	 */
	@Override
	@Raw
	public boolean canHaveAsWeapon(Weapon weapon) {
		if (isTerminated())
			return (weapon == null);
		return (weapon != null) && (weapon instanceof Rifle);
	}

	/**
	 * Variable registering the mass of this rifleBullet expressed in kilogram.
	 */
	private double mass = 0.010;
	
	/**
	 * Variable registering the force of this rifleBullet expressed in Newton.
	 */
	private double force = 1.5;
	
	/**
	 * Variable registering the shooting cost of this RifleBullet.
	 */
	private int shootingCost = 10;
	
	/**
	 * Variable registering the hit cost of this RifleBullet.
	 */
	private int hitCost = 20;
}
