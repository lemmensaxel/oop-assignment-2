package worms.model;

import java.io.File;
import java.util.Random;

import worms.gui.Level;

public class test {

	public static void main(String[] args) {
		Random random = new Random();
		Level level = new Level(new File("levels/Simple.lvl"));
		level.load();
		Facade facade = new Facade();
		World world = facade.createWorld(level.getWorldWidth(), level.getWorldHeight(), level.getPassableMap(), random);
		
		if (world.findRandomAdjacentLocation(Food.getRadius())) {
		}
		
		System.out.println(world.getAdjacentLocationX());
		System.out.println();
		System.out.println(world.getAdjacentLocationY());

	}

}
