package worms.model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.util.Random;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import worms.gui.Level;

public class World_test {

	public World_test() {
	}

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}
	
	// WIDTH & HEIGHT

	@Test
	public void getWidth_test() {
		Level level = new Level(new File("levels/Simple.lvl"));
		level.load();
		Facade facade = new Facade();
		World world = facade.createWorld(level.getWorldWidth(), level.getWorldHeight(), level.getPassableMap(), null);
		
		assertEquals(26.7, world.getWidth(), 0.1);
	}
	
	@Test
	public void getHeight_test() {
		Level level = new Level(new File("levels/Simple.lvl"));
		level.load();
		Facade facade = new Facade();
		World world = facade.createWorld(level.getWorldWidth(), level.getWorldHeight(), level.getPassableMap(), null);
		
		assertEquals(20.0, world.getHeight(), 0.1);
	}
	
	@Test
	public void isValidSizeOfWorld_LegalCase() {
		assertEquals(true, World.isValidSizeOfWorld(1, 1));
	}
	
	@Test
	public void isValidSizeOfWorld_IllegalCase() {
		assertEquals(false, World.isValidSizeOfWorld(-1, -1));
	}
	
	
	// TEAMS
	
	@Test
	public void addTeam_LegalCase() {
		Level level = new Level(new File("levels/Simple.lvl"));
		level.load();
		Facade facade = new Facade();
		World world = facade.createWorld(level.getWorldWidth(), level.getWorldHeight(), level.getPassableMap(), null);
		
		Team team = new Team("HappyTesters", world);
		world.addTeam(team);

	}
	
	@Test
	public void hasAsTeam_LegalCase() {
		Level level = new Level(new File("levels/Simple.lvl"));
		level.load();
		Facade facade = new Facade();
		World world = facade.createWorld(level.getWorldWidth(), level.getWorldHeight(), level.getPassableMap(), null);
		Team team = new Team("HappyTesters", world);
		assertTrue(world.hasAsTeam(team));
	}
	
	@Test
	public void hasAsTeam_IllegalCase() {
		Level level = new Level(new File("levels/Simple.lvl"));
		level.load();
		Facade facade = new Facade();
		World world = facade.createWorld(level.getWorldWidth(), level.getWorldHeight(), level.getPassableMap(), null);
		
		Level level2 = new Level(new File("levels/Simple.lvl"));
		level2.load();
		Facade facade2 = new Facade();
		World world2 = facade.createWorld(level.getWorldWidth(), level.getWorldHeight(), level.getPassableMap(), null);
		
		Team team = new Team("HappyTesters", world);
		Team team2 = new Team("HappyTester", world2);
		assertFalse(world.hasAsTeam(team2));
	}
	
	// FOOD
	
	@Test
	public void canHasAsFood_IllegalCase() {
		Level level = new Level(new File("levels/Simple.lvl"));
		level.load();
		Facade facade = new Facade();
		Random random = new Random();
		World world = facade.createWorld(level.getWorldWidth(), level.getWorldHeight(), level.getPassableMap(), random);
		
		Food food = null;
		if (world.findRandomAdjacentLocation(Food.getRadius())) {
			food = new Food(world.getAdjacentLocationX(), world.getAdjacentLocationY(), world);
		}
		
		assertFalse(world.canHaveAsFood(food));
	}
	
	@Test
	public void canHasAsFood_IllegalCase2() {
		Level level = new Level(new File("levels/Simple.lvl"));
		level.load();
		Facade facade = new Facade();
		World world = facade.createWorld(level.getWorldWidth(), level.getWorldHeight(), level.getPassableMap(), null);
		
		Food food = null;
		
		assertFalse(world.canHaveAsFood(food));
	}
	
	@Test
	public void canHasAsFood_IllegalCase3() {
		Random random = new Random();
		Level level = new Level(new File("levels/Simple.lvl"));
		level.load();
		Facade facade = new Facade();
		World world = facade.createWorld(level.getWorldWidth(), level.getWorldHeight(), level.getPassableMap(), random);
		
		Level level2 = new Level(new File("levels/Skulls.lvl"));
		level2.load();
		Facade facade2 = new Facade();
		World world2 = facade2.createWorld(level.getWorldWidth(), level2.getWorldHeight(), level2.getPassableMap(), random);
		
		
		if (world2.findRandomAdjacentLocation(Food.getRadius())) {
		}
		Food food = new Food(world2.getAdjacentLocationX(), world2.getAdjacentLocationY(), world2);
		
		assertFalse(world.canHaveAsFood(food));
	}
	
	@Test
	public void hasAsFood_LegalCase() {
		Random random = new Random();
		Level level = new Level(new File("levels/Simple.lvl"));
		level.load();
		Facade facade = new Facade();
		World world = facade.createWorld(level.getWorldWidth(), level.getWorldHeight(), level.getPassableMap(), random);
		
		if (world.findRandomAdjacentLocation(Food.getRadius())) {
		}
		
		Food food = new Food(world.getAdjacentLocationX(), world.getAdjacentLocationY(), world);
		
		assertTrue(world.hasAsFood(food)); 
	}
	
	@Test
	public void hasAsFood_IllegalCase() {
		Random random1 = new Random();
		Random random2 = new Random();
		Level level = new Level(new File("levels/Simple.lvl"));
		level.load();
		Facade facade = new Facade();
		World world = facade.createWorld(level.getWorldWidth(), level.getWorldHeight(), level.getPassableMap(), random1);
		
		Level level2 = new Level(new File("levels/Skulls.lvl"));
		level2.load();
		Facade facade2 = new Facade();
		World world2 = facade2.createWorld(level.getWorldWidth(), level2.getWorldHeight(), level2.getPassableMap(), random2);
		
		
		Food food = null;
		if (world2.findRandomAdjacentLocation(Food.getRadius())) {
			food = new Food(world2.getAdjacentLocationX(), world2.getAdjacentLocationY(), world2);
		}
		
		assertFalse(world.hasAsFood(food));
	}
	

	@Test
	public void removeFood_LegalCase() {
		Random random = new Random();
		Level level = new Level(new File("levels/Simple.lvl"));
		level.load();
		Facade facade = new Facade();
		World world = facade.createWorld(level.getWorldWidth(), level.getWorldHeight(), level.getPassableMap(), random);
		
		if (world.findRandomAdjacentLocation(Food.getRadius())) {
		}
		
		Food food = new Food(world.getAdjacentLocationX(), world.getAdjacentLocationY(), world);
		food.terminate();
		assertFalse(world.hasAsFood(food)); 
	}
	
	
	
	// WORMS
	
	@Test
	public void hasAsWorm_LegalCase() {
		Random random = new Random();
		Level level = new Level(new File("levels/Simple.lvl"));
		level.load();
		Facade facade = new Facade();
		World world = facade.createWorld(level.getWorldWidth(), level.getWorldHeight(), level.getPassableMap(), random);
		
		if (world.findRandomAdjacentLocation(0.50)) {
		}
		Worm worm = new Worm(world.getAdjacentLocationX(), world.getAdjacentLocationY(), 1, 0.50, "Kiki", world);
		
		assertTrue(world.hasAsWorm(worm));
		
	}
	
	@Test
	public void hasAsWorm_IllegalCase() {
		Random random = new Random();
		Level level = new Level(new File("levels/Simple.lvl"));
		level.load();
		Facade facade = new Facade();
		World world = facade.createWorld(level.getWorldWidth(), level.getWorldHeight(), level.getPassableMap(), random);
		
		Level level2 = new Level(new File("levels/Skulls.lvl"));
		level2.load();
		Facade facade2 = new Facade();
		World world2 = facade.createWorld(level2.getWorldWidth(), level2.getWorldHeight(), level2.getPassableMap(), random);

		if (world2.findRandomAdjacentLocation(0.50)) {
		}
		
		Worm worm = new Worm(world2.getAdjacentLocationX(), world2.getAdjacentLocationY(), 1, 0.50, "Kiki", world2);
		
		assertFalse(world.hasAsWorm(worm));
		
	}
	
	// LOCATION
	
	@Test 
	public void isAdjacentToImpassableTerrain_TestCase() {
		Random random = new Random();
		Level level = new Level(new File("levels/Simple.lvl"));
		level.load();
		Facade facade = new Facade();
		World world = facade.createWorld(level.getWorldWidth(), level.getWorldHeight(), level.getPassableMap(), random);
		if(world.findRandomAdjacentLocation(0.50));
		assertTrue(world.isAdjacentToImpassableTerrain(world.getAdjacentLocationX(), world.getAdjacentLocationY(), 0.50));
	}
	
	@Test
	public void isImpassable_TestCase() {
		Random random = new Random();
		Level level = new Level(new File("levels/Simple.lvl"));
		level.load();
		Facade facade = new Facade();
		World world = facade.createWorld(level.getWorldWidth(), level.getWorldHeight(), level.getPassableMap(), random);
		
		assertTrue(world.isImpassable(0, 0, 0.50));
	}
	
	@Test
	public void isPassable() {
		Random random = new Random();
		Level level = new Level(new File("levels/Simple.lvl"));
		level.load();
		Facade facade = new Facade();
		World world = facade.createWorld(level.getWorldWidth(), level.getWorldHeight(), level.getPassableMap(), random);
		if(world.findRandomAdjacentLocation(0.50));
		assertTrue(world.isPassable(world.getAdjacentLocationX(), world.getAdjacentLocationY(), 0.50));
	}
	
	// GAME
	
	@Test
	public void setCurrentWorm_getCurrentWorm_TestCase() {
		Random random = new Random();
		Level level = new Level(new File("levels/Simple.lvl"));
		level.load();
		Facade facade = new Facade();
		World world = facade.createWorld(level.getWorldWidth(), level.getWorldHeight(), level.getPassableMap(), random);
		
		if (world.findRandomAdjacentLocation(0.50)) {
		}
		
		Worm worm = new Worm(world.getAdjacentLocationX(), world.getAdjacentLocationY(), 1, 0.50, "Kiki", world);
		
		world.setCurrentWorm(worm);
		
		assertEquals(worm, world.getCurrentWorm());
	}
	

}
