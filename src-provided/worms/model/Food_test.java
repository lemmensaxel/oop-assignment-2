package worms.model;

import static org.junit.Assert.*;

import java.io.File;
import java.util.Random;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import worms.gui.Level;

public class Food_test {

	public Food_test() {
	}

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void isValidCoordinate_TestCase() {
		assertTrue(Food.isValidCoordinate(1.0));
	}
	
	// POSITION
	
	@Test
	public void setXCoordinate_TestCase() {
		Level level = new Level(new File("levels/Simple.lvl"));
		level.load();
		Facade facade = new Facade();
		Random random = new Random();
		World world = facade.createWorld(level.getWorldWidth(), level.getWorldHeight(), level.getPassableMap(), random);
		
		if (world.findRandomAdjacentLocation(Food.getRadius())) {
		}
		
		Food food = new Food(world.getAdjacentLocationX(), world.getAdjacentLocationY(), world);
		food.setXCoordinate(3.0);
		
		assertEquals(3.0, food.getCoordinateX(), 0.01);
	}
	
	@Test
	public void setYCoordinate_TestCase() {
		Level level = new Level(new File("levels/Simple.lvl"));
		level.load();
		Facade facade = new Facade();
		Random random = new Random();
		World world = facade.createWorld(level.getWorldWidth(), level.getWorldHeight(), level.getPassableMap(), random);
		
		if (world.findRandomAdjacentLocation(Food.getRadius())) {
		}
		
		Food food = new Food(world.getAdjacentLocationX(), world.getAdjacentLocationY(), world);
		food.setYCoordinate(11.0);
		
		assertEquals(11.0, food.getCoordinateY(), 0.01);
	}
	
	// WORLD
	
	@Test
	public void canHaveAsWorld_LegalCase() {
		Level level = new Level(new File("levels/Simple.lvl"));
		level.load();
		Facade facade = new Facade();
		Random random = new Random();
		World world = facade.createWorld(level.getWorldWidth(), level.getWorldHeight(), level.getPassableMap(), random);
		
		if (world.findRandomAdjacentLocation(Food.getRadius())) {
		}
		Food food = new Food(world.getAdjacentLocationX(), world.getAdjacentLocationY(), world);
		
		assertEquals(true, food.canHaveAsWorld(world));
	}
	
	@Test
	public void hasProperWorld_TestCase() {
		Level level = new Level(new File("levels/Simple.lvl"));
		level.load();
		Facade facade = new Facade();
		Random random = new Random();
		World world = facade.createWorld(level.getWorldWidth(), level.getWorldHeight(), level.getPassableMap(), random);
		
		if (world.findRandomAdjacentLocation(Food.getRadius())) {
		}
		Food food = new Food(world.getAdjacentLocationX(), world.getAdjacentLocationY(), world);
		
		assertTrue(food.hasProperWorld());
	}
	
	// TERMINATE
	
	@Test
	public void isTerminated_TestCase() {
		Level level = new Level(new File("levels/Simple.lvl"));
		level.load();
		Facade facade = new Facade();
		Random random = new Random();
		World world = facade.createWorld(level.getWorldWidth(), level.getWorldHeight(), level.getPassableMap(), random);
		
		if (world.findRandomAdjacentLocation(Food.getRadius())) {
		}
		Food food = new Food(world.getAdjacentLocationX(), world.getAdjacentLocationY(), world);
		
		assertFalse(food.isTerminated());
	}

}
