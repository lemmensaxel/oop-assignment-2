package worms.model;
import be.kuleuven.cs.som.annotate.Basic;
import be.kuleuven.cs.som.annotate.Raw;

/**
 * A class representing the food of a world. 
 * 
 * @invar	Each food must have a valid x-coordinate.
 *       	| isValidCoordinate(getXCoordinate())
 * @invar	Each food must have a valid y-coordinate.
 *       	| isValidCoordinate(getYCoordinate())
 * @invar  	Each food must have a proper world.
 *       	| hasProperWorld()
 * 
 * @version 2.0
 * @author  Axel Lemmens & Maarten Rimaux
 */
public class Food {
	
	// CONSTRUCTOR
	
	/**
	 * @param	xCoordinate
	 * 			The provided value for the x-coordinate in meters.
	 * @param	yCoordinate
	 * 			The provided value for the y-coordinate in meters.
	 * @param	world
	 * 			The provided value for the world of this new food.
	 * @effect 	The x-coordinate for this new food is set to the provided value xCoordinate.
	 *      	| setXCoordinate(xCoordinate)
	 * @effect 	The y-coordinate for this new food is set to the provided value yCoordinate.
	 *      	| setYCoordinate(yCoordinate)
	 * @post	The new world of this new food is equal to the provided "world"
	 * 			| new.getWorld() == world
	 * @post   	The number of food for the given world is
	 *         	incremented by 1.
	 *       	| (new world).getFood().size() == world.getFood().size() + 1
	 * @post   	The given world has this new food as its very last food.
	 *      	| (new world).getFood().get(world.getFood().size()+1) == this
	 * @throws	IllegalArgumentException
	 * 			An exception is thrown when the provided value is not a valid coordinate.
	 * 			| ! isValidCoordinate(xCoordinate)
	 * @throws	IllegalArgumentException
	 * 			An exception is thrown when the provided value is not a valid coordinate.
	 * 			| ! isValidCoordinate(yCoordinate)
	 * @throws	IllegalArgumentException
	 * 			An exception is thrown when this new food cannot have the given world as
	 *         	its world.
	 * 			| ! canHaveAsWorld(world)
	 */
	public Food(double xCoordinate, double yCoordinate, World world) throws IllegalArgumentException {
		setXCoordinate(xCoordinate);
		setYCoordinate(yCoordinate);
		setWorld(world);
		world.addFood(this);
	}
	
	// POSITION
	
	/**
	 * A method to return the x-coordinate of this food item.
	 */
	public double getCoordinateX() {
		return xCoordinate;
	}
	
	/**
	 * A method to return the y-coordinate of this food item.
	 */
	public double getCoordinateY() {
		return yCoordinate;
	}

	/**
	 * A method to set the x-coordinate of the position of this food item.
	 * 
	 * @param	xCoordinate
	 * 			The new x-coordinate of the position of this food.
	 * @post	The new value of the xCoordinate is set to the provided value.
	 * 			| new.getCoordinateX() == xCoordinate
	 * @throws	IllegalArgumentException
	 * 			An exception is thrown when the provided value is not a valid coordinate.
	 * 			| ! isValidCoordinate(xCoordinate) 
	 */
	public void setXCoordinate(double xCoordinate) throws IllegalArgumentException {
		if (! isValidCoordinate(xCoordinate))
			throw new IllegalArgumentException();
		this.xCoordinate = xCoordinate;
	}
	
	/**
	 * A method to set the y-coordinate of the position of this food.
	 * 
	 * @param 	yCoordinate
	 * 			The new y-coordinate of the position of this food.
	 * @post	The new value of the yCoordinate is set to the provided value.
	 * 			| new.getCoordinateY() == yCoordinate
	 * @throws	IllegalArgumentException
	 * 			An exception is thrown when the provided value is not a valid coordinate.
	 * 			| ! isValidCoordinate(yCoordinate)
	 */
	public void setYCoordinate(double yCoordinate) throws IllegalArgumentException { 
		if (! isValidCoordinate(yCoordinate))
			throw new IllegalArgumentException();	
		this.yCoordinate = yCoordinate;
		
	}
	
	/**
	 * A method to check whether a coordinate is valid.
	 * 
	 * @param 	aCoordinate
	 * 			The coordinate to check.
	 * @return 	The result should be a valid number.
	 * 			| result == !Double.isNaN(aCoordinate) 
	 * 			| && (aCoordinate < Double.POSITIVE_INFINITY)
	 * 			| && (aCoordinate > Double.NEGATIVE_INFINITY)
	 */
	public static boolean isValidCoordinate(double aCoordinate) { 
		return (!Double.isNaN(aCoordinate) 
				&& (aCoordinate < Double.POSITIVE_INFINITY) 
				&& (aCoordinate > Double.NEGATIVE_INFINITY));									 
	}
	
	/**
	 * Variable registering the x-coordinate of the position of this food.
	 */
	private double xCoordinate;
	
	/**
	 * Variable registering the y-coordinate of the position of this food.
	 */
	private double yCoordinate;
	
	// RADIUS
	
	/**
	 * A method to return the radius of this food item.
	 */
	public static double getRadius(){
		return 0.20;
	}
	
	// EATEN
	
	/**
	 * A method to check whether this food is active or not.
	 * 
	 * @post	If the result is false and only if it is false, this
	 * 			projectile is terminated.
	 * 			| new.isTerminated() == true
	 * @return	The result is true if this food has not been eaten by a worm.
	 * 			|for each I in 0..this.getWorld().getWorms().size()-1
	 * 			|	if( (Math.sqrt( Math.pow(this.getCoordinateX() - this.getWorld().getWorms().get(i).getCoordinateX(), 2) +
	 *			|			    	Math.pow(this.getCoordinateY() - this.getWorld().getWorms().get(i).getCoordinateY(), 2) )) 
	 *			|		<= getRadius() + 0.2 )
	 *			|			then result == false
	 *			| result == true
	 */
	public boolean isActive(){
		for(int i = 0; i < this.getWorld().getWorms().size(); i++) {
			double distanceBetweenWormAndFood = Math.sqrt( Math.pow(this.getCoordinateX() - this.getWorld().getWorms().get(i).getCoordinateX(), 2) +
														   Math.pow(this.getCoordinateY() - this.getWorld().getWorms().get(i).getCoordinateY(), 2) );
			if( distanceBetweenWormAndFood < this.getWorld().getWorms().get(i).getRadius() ){
				this.getWorld().getWorms().get(i).setRadius(this.getWorld().getWorms().get(i).getRadius()+
														   (this.getWorld().getWorms().get(i).getRadius()/10));
				this.terminate();
				return false;
			}
		}
		return true;
	}
	
	// WORLD
	
	/** 
	 * Return the current world of this food item.
	 */
	@Basic @Raw 
	public World getWorld() {
		return this.world;
	}
	
	/**
	 * Check whether this food can have the given world as its current world.
	 * 
	 * @param	world
	 *          The world to check.
	 * @return	If this food is not yet terminated, true if and
	 *          only if the given world is effective.
	 *        	| if (! isTerminated())
	 *       	|   then result == (world != null) 
	 * @return	If this food is terminated, true if and only if
	 *          the given world is not effective.
	 *        	| if (this.isTerminated())
	 *       	|   then result == (world == null)
	 */
	@Raw
	public boolean canHaveAsWorld(World world) {
		if (isTerminated())
			return (world == null);
		return (world != null);
	}
	
	/**
	 * Check whether this food item has a proper world.
	 * 
	 * @return	True if and only if this food can have its world as its
	 * 			world, and if this food is terminated or the world of
	 *          this food has this food as one of its food.
	 *       	| result ==
	 *        	|   canHaveAsWorld(getWorld()) &&
	 *       	|   ( isTerminated() || getWorld().hasAsFood(this))
	 */
	public boolean hasProperWorld() {
		return canHaveAsWorld(getWorld()) &&
				 ( isTerminated() || getWorld().hasAsFood(this));
	}
	
	/** 
	 * Register the given world as the world of this food.
	 * 
	 * @param	world
	 *          The world to be registered as the world of this food.
	 * @post   	The world of this food is the same as the given world.
	 *       	| new.getWorld() == world
	 * @throws	IllegalArgumentException
	 * 			An exception is thrown when the provided world is not valid.
	 * 			| ! canHaveAsWorld(world)
	 */
	@Raw
	private void setWorld(World world) throws IllegalArgumentException {
		if( !canHaveAsWorld(world) )
			throw new IllegalArgumentException("The provided world is not a valid world.");
		this.world = world;
	}
	
	/**
	 * Variable referencing the world of this food.
	 */
	private World world;
	
	// TERMINATE
	
	/**
	 * Check whether this food item is terminated.
	 */
	@Basic @Raw
	public boolean isTerminated() {
		return this.getState() == State.TERMINATED;
	}
	
	/**
	 * Terminate this food item.
	 * 
	 * @post	This food is terminated.
	 *       	| new.isTerminated()
	 * @post   	If this food was not yet terminated, this food
	 *        	is no longer one of the food of the world to which
	 *        	this food belonged.
	 *     		| if (! isTerminated())
	 *    	    |   then ! (new getWorld()).hasAsFood(this))
	 * @post  	If this food was not yet terminated, the number of
	 *      	food of the world to which this food belonged is
	 *        	decremented by 1.
	 *     		| if (! isTerminated())
	 *      	|   then (new getWorld()).getFood().size() ==
	 *      	|            this.getWorld().getFood().size() - 1
	 * @post  	If this food was not yet terminated, all food
	 *        	of the world to which this food belonged registered at an
	 *        	index beyond the index at which this food was registered,
	 *        	are shifted one position to the left.
	 *       	| for each I,J in ...getWorld().getFood().size()-1:
	 *       	|   if ( (getWorld().getFood().get(I) == food) and (I < J) )
	 *       	|     then (new getWorld()).getFood().get(J-1) == getWorld().getFood().get(J)
	 * 
	 */
	public void terminate() {
		if (!isTerminated()) {
			setState(State.TERMINATED);
			World oldWorld = getWorld();
			this.setWorld(null);
			oldWorld.removeFood(this);
		}
	}
	
	// STATE
	
	/**
	 * Enumeration of all possible states of this food item.
	 */
	private static enum State {
		ACTIVE, TERMINATED;
	}
	
	/**
	 * Return the state of this food item.
	 */
	@Raw
	private State getState() {
		return this.state;
	}

	/**
	 * Set the state of this food item to the given state.
	 * 
	 * @param	state
	 *        	The new state for this food.
	 * @pre		The given state must exists.
	 *       	| state != null
	 * @post	The state of this food is the same as the given state.
	 *     		| new.getState() == state
	 */
	private void setState(State state) {
		assert (state != null);
		this.state = state;
	}

	/**
	 * Variable registering the state of this food.
	 */
	private State state = State.ACTIVE;

}
