package worms.model;

import static org.junit.Assert.*;

import java.io.File;
import java.util.Random;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import worms.gui.Level;

public class Team_test {

	public Team_test() {
	}

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	// NAME
	
	@Test
	public void getName_TestCase() {
		Level level = new Level(new File("levels/Simple.lvl"));
		level.load();
		Facade facade = new Facade();
		World world = facade.createWorld(level.getWorldWidth(), level.getWorldHeight(), level.getPassableMap(), null);
		Team team = new Team("TheHappyTesters", world);
		assertEquals("TheHappyTesters", team.getName());
	}
	
	@Test
	public void isValidName_LegalCase() {
		assertEquals(true, Team.isValidName("Axe"));
	}
	
	@Test
	public void isValidName_IllegalCase1() {
		assertEquals(false, Team.isValidName("axe"));
	}
	
	@Test
	public void isValidName_IllegalCase2() {
		assertEquals(false, Team.isValidName("Axe5"));
	}
	
	@Test
	public void isValidName_IllegalCase3() {
		assertEquals(false, Team.isValidName("A"));
	}
	
	@Test
	public void isValidName_IllegalCase4() {
		assertEquals(false, Team.isValidName("+++++=====//////"));
	}
	
	@Test
	public void isValidName_IllegalCase5() {
		assertEquals(false, Team.isValidName(""));
	}
	
	@Test
	public void setName_LegalCase() {
		Level level = new Level(new File("levels/Simple.lvl"));
		level.load();
		Facade facade = new Facade();
		World world = facade.createWorld(level.getWorldWidth(), level.getWorldHeight(), level.getPassableMap(), null);
		Team team = new Team("TheHappyTesters", world);
		team.setName("Testers");
		assertEquals("Testers", team.getName());
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void setName_IllegalCase() {
		Level level = new Level(new File("levels/Simple.lvl"));
		level.load();
		Facade facade = new Facade();
		World world = facade.createWorld(level.getWorldWidth(), level.getWorldHeight(), level.getPassableMap(), null);
		Team team = new Team("TheHappyTesters", world);
		team.setName("happytesters");
	}
	
	// WORM
	
	
	
	@Test
	public void canHaveAsWorm_testCase() {
		Level level = new Level(new File("levels/Simple.lvl"));
		level.load();
		Facade facade = new Facade();
		Random random = new Random();
		World world = facade.createWorld(level.getWorldWidth(), level.getWorldHeight(), level.getPassableMap(), random);
		
		if (world.findRandomAdjacentLocation(0.50)) {
		}
		
		Worm testworm = new Worm(world.getAdjacentLocationX(), world.getAdjacentLocationY(), Math.PI/4, 0.50, "Kiki", world);
		Team happyTesters = new Team("TheHappyTesters", world);
		
		assertFalse(happyTesters.canHaveAsWorm(testworm));
	}
	
	
	
	// WORLD
	
	@Test
	public void getWorld_testCase() {
		Level level = new Level(new File("levels/Simple.lvl"));
		level.load();
		Facade facade = new Facade();
		World world = facade.createWorld(level.getWorldWidth(), level.getWorldHeight(), level.getPassableMap(), null);
		Team team = new Team("TheHappyTesters", world);		
		assertEquals(true, world == team.getWorld());
	}
	
	@Test
	public void canHaveAsWorld_legalcase() {
		Level level = new Level(new File("levels/Simple.lvl"));
		level.load();
		Facade facade = new Facade();
		World world = facade.createWorld(level.getWorldWidth(), level.getWorldHeight(), level.getPassableMap(), null);
		Team team = new Team("TheHappyTesters", world);
		assertEquals(true, team.canHaveAsWorld(world));
	}
	
	@Test
	public void hasProperWorld_legalcase() {
		Level level = new Level(new File("levels/Simple.lvl"));
		level.load();
		Facade facade = new Facade();
		World world = facade.createWorld(level.getWorldWidth(), level.getWorldHeight(), level.getPassableMap(), null);
		Team team = new Team("TheHappyTesters", world);
		assertEquals(true, team.hasProperWorld());
	}
	
	
	
}
