package worms.model;

import java.util.Collection;
import java.util.Random;


public class Facade implements IFacade{

	@Override
	public void addEmptyTeam(World world, String newName) {
		try {
			Team team = new Team(newName,world);
			} catch (Exception e) {
			    throw new ModelException(e);
			}
	}

	@Override
	public void addNewFood(World world) {
		try {
			if(world.findRandomAdjacentLocation(Food.getRadius())){
				Food food = new Food(world.getAdjacentLocationX(), world.getAdjacentLocationY(), world);
			}
			} catch (Exception e) {
			    throw new ModelException(e);
			}
	}

	@Override
	public void addNewWorm(World world) {
		try {
			if(world.findRandomAdjacentLocation(0.50)){
				Worm worm = new Worm(world.getAdjacentLocationX(), world.getAdjacentLocationY(), 1, 0.50, "New worm", world);
			}
			} catch (Exception e) {
			    throw new ModelException(e);
			}
	}

	@Override
	public boolean canFall(Worm worm) {
		try {
			return worm.canFall();
			} catch (Exception e) {
			    throw new ModelException(e);
			}
	}

	@Override
	public boolean canMove(Worm worm) {
		try {
			return worm.canMove();
			} catch (Exception e) {
			    throw new ModelException(e);
			}
	}

	@Override
	public boolean canTurn(Worm worm, double angle) {
		try {
			return worm.canTurn(angle);
			} catch (Exception e) {
			    throw new ModelException(e);
			}
	}

	@Override
	public Food createFood(World world, double x, double y) {
		try {
				Food food = new Food(x, y, world);
				return food;
			} catch (Exception e) {
			    throw new ModelException(e);
			}
		
	}
	
	@Override
	public World createWorld(double width, double height,
			boolean[][] passableMap, Random random) {
		try {
			World world = new World(width, height, passableMap, random);
			return world;
			} catch (Exception e) {
			    throw new ModelException(e);
			}
	}

	@Override
	public Worm createWorm(World world, double x, double y, double direction,
			double radius, String name) {
		try {
			Worm worm = new Worm(x, y, direction, radius, name, world);
			return worm;
		} catch (Exception e) {
		    throw new ModelException(e);
		}
	}

	@Override
	public void fall(Worm worm) {
		try {
			worm.fall();
			} catch (Exception e) {
			    throw new ModelException(e);
			}
	}

	@Override
	public int getActionPoints(Worm worm) {
		try {
			return worm.getCurrentActionPoints();
			} catch (Exception e) {
			    throw new ModelException(e);
			}
	}

	@Override
	public Projectile getActiveProjectile(World world) {
		try {
			return world.getActiveProjectile();
			} catch (Exception e) {
			    throw new ModelException(e);
			}
	}

	@Override
	public Worm getCurrentWorm(World world) {
		try {
			return world.getCurrentWorm();
			} catch (Exception e) {
			    throw new ModelException(e);
			}
	}

	@Override
	public Collection<Food> getFood(World world) {
		try {
			return world.getFood();
			} catch (Exception e) {
			    throw new ModelException(e);
			}
	}

	@Override
	public int getHitPoints(Worm worm) {
		try {
			return worm.getCurrentHitPoints();
			} catch (Exception e) {
			    throw new ModelException(e);
			}
	}

	@Override
	public double[] getJumpStep(Projectile projectile, double t) {
		try {
			return projectile.shootStep(t);
			} catch (Exception e) {
			    throw new ModelException(e);
			}
		
	}

	@Override
	public double[] getJumpStep(Worm worm, double t) {
		try {
			return worm.jumpStep(t);
			} catch (Exception e) {
			    throw new ModelException(e);
			}
	}

	@Override
	public double getJumpTime(Projectile projectile, double timeStep) {
		try {
			return projectile.getShootTime(timeStep);
			} catch (Exception e) {
			    throw new ModelException(e);
			}
	}

	@Override
	public double getJumpTime(Worm worm, double timeStep) {
		try {
			return worm.getJumpTime(timeStep);
			} catch (Exception e) {
			    throw new ModelException(e);
			}
	}

	@Override
	public double getMass(Worm worm) {
		try {
			return worm.getMass();
			} catch (Exception e) {
			    throw new ModelException(e);
			}
	}

	@Override
	public int getMaxActionPoints(Worm worm) {
		try {
			return worm.getMaximumActionPoints();
			} catch (Exception e) {
			    throw new ModelException(e);
			}
	}

	@Override
	public int getMaxHitPoints(Worm worm) {
		try {
			return worm.getMaximumHitPoints();
			} catch (Exception e) {
			    throw new ModelException(e);
			}
	}

	@Override
	public double getMinimalRadius(Worm worm) {
		try {
			return worm.getLowerBoundRadius();
			} catch (Exception e) {
			    throw new ModelException(e);
			}
	}

	@Override
	public String getName(Worm worm) {
		try {
			return worm.getName();
			} catch (Exception e) {
			    throw new ModelException(e);
			}
	}

	@Override
	public double getOrientation(Worm worm) {
		try {
			return worm.getDirection();
			} catch (Exception e) {
			    throw new ModelException(e);
			}
	}

	@Override
	public double getRadius(Food food) {
		try {
			return Food.getRadius();
			} catch (Exception e) {
			    throw new ModelException(e);
			}
	}

	@Override
	public double getRadius(Projectile projectile) {
		try {
			return projectile.getRadius();
			} catch (Exception e) {
			    throw new ModelException(e);
			}
	}

	@Override
	public double getRadius(Worm worm) {
		try {
			return worm.getRadius();
			} catch (Exception e) {
			    throw new ModelException(e);
			}
	}

	@Override
	public String getSelectedWeapon(Worm worm) {
		try {
			return worm.getSelectedWeapon().getName();
			} catch (Exception e) {
			    throw new ModelException(e);
			}
	}

	@Override
	public String getTeamName(Worm worm) {
		try {
			return worm.getTeam().getName();
			} catch (Exception e) {
			    throw new ModelException(e);
			}
	}

	@Override
	public String getWinner(World world) {
		try {
			return world.getWinner();
			} catch (Exception e) {
			    throw new ModelException(e);
			}
	}

	@Override
	public Collection<Worm> getWorms(World world) {
		try {
			return world.getWorms();
			} catch (Exception e) {
			    throw new ModelException(e);
			}
	}

	@Override
	public double getX(Food food) {
		try {
			return food.getCoordinateX();
			} catch (Exception e) {
			    throw new ModelException(e);
			}
	}

	@Override
	public double getX(Projectile projectile) {
		try {
			return projectile.getCoordinateX();
			} catch (Exception e) {
			    throw new ModelException(e);
			}
	}

	@Override
	public double getX(Worm worm) {
		try {
			return worm.getCoordinateX();
			} catch (Exception e) {
			    throw new ModelException(e);
			}
	}

	@Override
	public double getY(Food food) {
		try {
			return food.getCoordinateY();
			} catch (Exception e) {
			    throw new ModelException(e);
			}
	}

	@Override
	public double getY(Projectile projectile) {
		try {
			return projectile.getCoordinateY();
			} catch (Exception e) {
			    throw new ModelException(e);
			}
	}

	@Override
	public double getY(Worm worm) {
		try {
			return worm.getCoordinateY();
			} catch (Exception e) {
			    throw new ModelException(e);
			}
	}

	@Override
	public boolean isActive(Food food) {
		try {
			return food.isActive();
			} catch (Exception e) {
			    throw new ModelException(e);
			}
	}

	@Override
	public boolean isActive(Projectile projectile) {
		try {
			return projectile.isActive();
			} catch (Exception e) {
			    throw new ModelException(e);
			}
	}

	@Override
	public boolean isAdjacent(World world, double x, double y, double radius) {
		try {
			return world.isAdjacentToImpassableTerrain(x, y, radius);
			} catch (Exception e) {
			    throw new ModelException(e);
			}
	}

	@Override
	public boolean isAlive(Worm worm) {
		try {
			return worm.isAlive();
			} catch (Exception e) {
			    throw new ModelException(e);
			}
	}

	@Override
	public boolean isGameFinished(World world) {
		try {
			return world.isGameFinished();
			} catch (Exception e) {
			    throw new ModelException(e);
			}
	}

	@Override
	public boolean isImpassable(World world, double x, double y, double radius) {
		try {
			return world.isImpassable(x, y, radius);
			} catch (Exception e) {
			    throw new ModelException(e);
			}
	}

	@Override
	public void jump(Projectile projectile, double timeStep) {
		try {
			projectile.shoot(timeStep);
			} catch (Exception e) {
			    throw new ModelException(e);
			}
		
	}

	@Override
	public void jump(Worm worm, double timeStep) {
		try {
			worm.jump(timeStep);
			} catch (Exception e) {
			    throw new ModelException(e);
			}
	}

	@Override
	public void move(Worm worm) {
		try {
			worm.move();
			} catch (Exception e) {
			    throw new ModelException(e);
			}
		
	}

	@Override
	public void rename(Worm worm, String newName) {
		try {
			worm.setName(newName);
			} catch (Exception e) {
			    throw new ModelException(e);
			}
	}

	@Override
	public void selectNextWeapon(Worm worm) {
		try {
			worm.selectNextWeapon();
			} catch (Exception e) {
			    throw new ModelException(e);
			}
		
	}

	@Override
	public void setRadius(Worm worm, double newRadius) {
		try {
			worm.setRadius(newRadius);
			} catch (Exception e) {
			    throw new ModelException(e);
			}
	}

	@Override
	public void shoot(Worm worm, int yield) {
		try {
			worm.shoot(yield);
			} catch (Exception e) {
			    throw new ModelException(e);
			}
	}

	@Override
	public void startGame(World world) {
		try {
			world.startGame();
			} catch (Exception e) {
			    throw new ModelException(e);
			}
	}

	@Override
	public void startNextTurn(World world) {
		try {
			world.startNextTurn();
			} catch (Exception e) {
			    throw new ModelException(e);
			}
	}

	@Override
	public void turn(Worm worm, double angle) {
		try {
			worm.turn(angle);
			} catch (Exception e) {
			    throw new ModelException(e);
			}
	}
	
}