package worms.model;

import be.kuleuven.cs.som.annotate.Basic;
import be.kuleuven.cs.som.annotate.Raw;

/**
 * A class of BazookaBullets, a subclass of Projectile.
 *
 * @version  2.0
 * @author   Axel Lemmens & Maarten Rimaux
 */
public class BazookaBullet extends Projectile {
	
	/**
	 * A method to initialise this BazookaBullet with the given parameters.
	 * 
	 * @param	propulsionYield
	 * 			The propulsion yield for the BazookaBullet.
	 * @effect	The force is set depending on the given propulsion yield.
	 * 			| setForce(propulsionYield)
	 */
	public BazookaBullet(double xCoordiante, double yCoordinate, double direction,
			Weapon weapon, World world, int propulsionYield) throws IllegalArgumentException {
		super(xCoordiante, yCoordinate, direction, weapon, world);
		setForce(propulsionYield);
	}

	/**
	 * A method to return the current radius of this BazookaBullet expressed in meters.
	 * 
	 * @return	The radius of this BazookaBullet.
	 * 			| return == Math.cbrt( ( getMass()/( getDensity()*(4.0/3.0)*(Math.PI) ) ) )
	 */
	@Override
	@Basic
	public double getRadius() {
		return Math.cbrt( ( getMass()/( getDensity()*(4.0/3.0)*(Math.PI) ) ) );
	}

	/**
	 * A method to return the mass of this BazookaBullet expressed in kilogram.
	 */
	@Override
	@Basic
	public double getMass() {
		return mass;
	}

	/**
	 * A method to return the lower bound force of this BazookaBullet expressed in Newton.
	 */
	@Basic
	public double getLowerBoundForce() {
		return lowerBoundForce;
	}
	
	/**
	 * A method to return the upper bound force of this BazookaBullet expressed in Newton.
	 */
	@Basic
	public double getUpperBoundForce() {
		return upperBoundForce;
	}
	
	/**
	 * A method to return the force of this BazookaBullet expressed in Newton.
	 */
	@Override
	@Basic
	public double getForce() {
		return force;
	}
	
	/**
	 * A method to set the force depending on the propulsionYield.
	 * 
	 * @param	propulsionYield
	 * 			The propulsion yield for this BazookaBullet.
	 * @post	The force is set depending on the propulsion yield.
	 * 			| force == propulsionYield/(100/getLowerBoundForce()) 
	 */
	@Basic
	public void setForce(int propulsionYield){
		force = propulsionYield/(100/getLowerBoundForce());
	}
	
	/**
	 * A method to return the shooting cost of this BazookaBullet.
	 */
	@Override
	@Basic
	public int getShootingCost() {
		return shootingCost;
	}

	/**
	 * A method to return the hit cost of this BazookaBullet.
	 */
	@Override
	@Basic
	public int getHitCost() {
		return hitCost;
	}
	
	/**
	 * A method to check whether this BazookaBullet can be shot.
	 * 
	 * @return 		The method returns true if the current action points of the worm of the weapon of this BazookaBullet
	 * 				are bigger than or equal to the shooting cost of this BazookaBullet and if the worm of the weapon of
	 * 				this BazookaBullet is located on passable terrain.
	 * 				| result == (this.getWeapon().getWorm().getCurrentActionPoints() >= this.getShootingCost()) 
	 * 				| && (this.getWorld().isPassable(this.getWeapon().getWorm().getCoordinateX(), this.getWeapon().getWorm().getCoordinateY(), this.getWeapon().getWorm().getRadius()))
	 */
	@Override
	public boolean canShoot(){
		return (this.getWeapon().getWorm().getCurrentActionPoints() >= this.getShootingCost()) 
				 && ( this.getWorld().isPassable(this.getWeapon().getWorm().getCoordinateX(), this.getWeapon().getWorm().getCoordinateY(), this.getWeapon().getWorm().getRadius()));
	}
	
	/**
	 * Check whether this BazookaBullet can have the given weapon as its current weapon.
	 * 
	 * @param	weapon
	 *          The weapon to check.
	 * @return	If this BazookaBullet is not yet terminated, true if and
	 *          only if the given weapon is effective and the class of
	 *          the weapon is equal to the Bazooka class.
	 *        	| if (! isTerminated())
	 *       	|   then result == (weapon != null) && (weapon instanceof Bazooka)
	 * @return	If this BazookaBullet is terminated, true if and only if
	 *          the given weapon is not effective.
	 *        	| if (! this.isTerminated())
	 *       	|   then result == (weapon == null)
	 */
	@Raw
	public boolean canHaveAsWeapon(Weapon weapon) {
		if (isTerminated())
			return (weapon == null);
		return (weapon != null) && (weapon instanceof Bazooka);
	}
	
	/**
	 * Variable registering the mass of this BazookaBullet expressed in kilogram.
	 */
	private double mass = 0.300;
	
	/**
	 * Variable registering the lower bound force of this BazookaBullet expressed in Newton.
	 */
	private double lowerBoundForce = 2.5;
	
	/**
	 * Variable registering the upper bound force of this BazookaBullet expressed in Newton.
	 */
	private double upperBoundForce = 9.5;
	
	/**
	 * Variable registering the force of this BazookaBullet expressed in Newton.
	 */
	private double force;
	
	/**
	 * Variable registering the shooting cost of this BazookaBullet.
	 */
	private int shootingCost = 50;
	
	/**
	 * Variable registering the hit cost of this BazookaBullet.
	 */
	private int hitCost = 80;
	
}
