package worms.model;

import java.util.ArrayList;
import java.util.Random;

import be.kuleuven.cs.som.annotate.Basic;
import be.kuleuven.cs.som.annotate.Raw;

/**
 * A class of worlds.
 * 
 * @invar	Each world must have a valid width and height.
 * 			| isValidSizeOfWorld(width, height)
 *
 * @version	2.0
 * @author Axel Lemmens & Maarten Rimaux 
 *
 */
public class World {
	
	//<------------------------------------------------------------------CONSTRUCTOR------------------------------------------------------------>
	
	/**
	 * Initialise this new world with the given parameters. 
	 *  
	 * @param 	width
	 * 			The width of this new world in meters.
	 * @param 	height
	 * 			The height of this new world in meters.
	 * @param	passableMap 
	 * 			A rectangular matrix indicating which parts of the terrain are passable and impassable. 
	 * @param	random 
	 * 			A random number generator, seeded with the value obtained from the command line or from GUIOptions,
	 *			that can be used to randomise aspects of this world in a repeatable way.
	 * @post	The width is set to the provided width.
	 * 			| new.getWidth() == width
	 * @post	The height is set to the provided height.
	 * 			| new.getHeight() == height
	 * @post	The passableMap is set to the provided passableMap.
	 * 			| new.getPassable() == passableMap
	 * @post	The random is set to the provided random.
	 * 			| new.getRandom() == random
	 * @throws 	IllegalArgumentException
	 * 			An exception is thrown when the provided width and height are not valid.
	 * 			| ! isValidSizeOfWorld(width,height)
	 */
	public World(double width, double height, boolean[][] passableMap, Random random) throws IllegalArgumentException {
		if (!isValidSizeOfWorld(width, height)) 
			throw new IllegalArgumentException("Invalid width or height.");
		this.width = width;
		this.height = height;
		this.passableMap = passableMap;
		this.random = random;
	}
	
	//<---------------------------------------------------------------WIDTH & HEIGHT--------------------------------------------------------->
	
	/**
	 * A method to return the width of the world in meters.
	 */
	@Basic
	public double getWidth() {
		return width;
	}
	
	/**
	 * A method to return the height of the world in meters.
	 */
	@Basic
	public double getHeight() {
		return height;
	}
	
	/**
	 * A method to check whether the size of this world is valid.
	 * 
	 * @param 	width
	 * 			The width to check.
	 * @param 	height
	 * 			The height to check.
	 * @return 	The result should be a width and a height between 0 and Double.MAX_VALUE (both inclusive).
	 * 		  	| result == (width >= 0) 
	 * 		  	| && (width <= Double.MAX_VALUE) 
	 * 		  	| && (height >= 0) 
	 * 		  	| && (height <= Double.MAX_VALUE)
	 */
	public static boolean isValidSizeOfWorld(double width, double height) { 
		return ((width >= 0) && (width <= Double.MAX_VALUE) 
				&& (height >= 0) && (height <= Double.MAX_VALUE));									 
	}
	
	/**
	 * Variable registering the width of this world in meters.
	 */
	private double width;
	
	/**
	 * Variable registering the height of this world in meters.
	 */
	private double height;
	
	//<----------------------------------------------TEAMS---------------------------------------------------->
	
	/**
	 * A method to return a list of all the teams in this world.
	 */
	@Basic
	public ArrayList<Team> getTeams(){
		return teamObjects;
	}
	
	/**
	 * A method to return the max number of teams this world can have.
	 */
	@Basic
	public int getMaxNumberOfTeams(){
		return 10;
	}
	
	/**
	 * A method to add a team to this world.
	 * 
	 * @param	team
	 * 			The team to be added.
	 * @pre		The given team is effective and already references
	 *     	    this world, and this world does not yet have the given
	 *        	team as one of its teams.
	 *      	| (team != null) && (team.getWorld() == this) &&
	 *      	| (! this.hasAsTeam(team))
	 * @post    The number of Teams for this world is
	 *          incremented by 1.
	 *       	| new.getTeams().size() == this.getTeams().size() + 1
	 * @post    A new Team is added to this world.
	 *       	| new.getTeams().get(getTeams().size()+1) == team
	 */
	@Basic
	public void addTeam(@Raw Team team){
		assert (team != null) && (team.getWorld() == this)
				&& (!this.hasAsTeam(team));
		teamObjects.add(team);
	}
	
	/**
	 * Check whether this world has the given team as one of its
	 * teams.
	 * 
	 * @param  	team
	 * 		   	The team to check.
	 * @return	The result is true when the provided team is a team of this world.
	 *       	| result ==  getTeams().contains(team)
	 */
	public boolean hasAsTeam(Team team) {
		return getTeams().contains(team);
	}
	
	/**
	 * Variable referencing a list collecting all the teams in this world.
	 */
	private ArrayList<Team> teamObjects = new ArrayList<Team>();
	
	//<----------------------------------------------------------FOOD----------------------------------------------------------->
	
	/**
	 * A method to return a list of all the food in this world.
	 */
	@Basic
	public ArrayList<Food> getFood() {
		return foodObjects;
	}
	
	/**
	 * Check whether this world can have the given food
	 * as one of its food.
	 * 
	 * @param	food
	 *        	The food to check.
	 * @return	True if and only if the given food is effective
	 *        	and already references this world, and this world
	 *        	does not yet have the given food as one of its food.
	 *       	| result ==
	 *       	|   (food != null) && (food.getWorld() == this)
				|		&& (!this.hasAsFood(food))
	 */
	@Raw
	public boolean canHaveAsFood(Food food) {
		return (food != null) && (food.getWorld() == this)
				&& (!this.hasAsFood(food));
	}
	
	/**
	 * Check whether this world has the given food as one of its
	 * food.
	 * 
	 * @param  	food
	 * 		   	The food to check.
	 * @return	The result is true when the provided food is a food of this world.
	 *       	| result == getFood().contains(food)
	 */
	public boolean hasAsFood(@Raw Food food) {
		return getFood().contains(food);
	}
	
	/**
	 * Add the given food to the list of food of this world.
	 * 
	 * @param	food
	 *         	The food to be added.
	 * @post	The number of food of this world is
	 *        	incremented by 1.
	 *      	| new.getFood().size() == this.getFood().size() + 1
	 * @post   	This world has the given food as its very last food.
	 *       	| new.getFood().get(getFood().size()+1) == food
	 * @throws	IllegalArgumentException
	 * 			An exception is thrown when the provided food is not a valid food.
	 * 			| ! canHaveAsFood(food)
	 * @throws	IllegalArgumentException
	 * 			An exception is thrown when u want to add a worm if the game is started.
	 * 			| isGameStarted()
	 */
	@Basic
	public void addFood(Food food) throws IllegalArgumentException{
		if(!canHaveAsFood(food))
			throw new IllegalArgumentException("The provided food is not a valid food.");
		if(isGameStarted())
			throw new IllegalArgumentException("The game is started u cannot add any food.");
		foodObjects.add(food);
	}
	
	/**
	 * 	A method to remove the given food from the list of food of this world.
	 * 
	 * @param	food
	 *         	The food to be removed.
	 * @post   	The number of food of this world is
	 *         	decremented by 1.
	 *       	| new.getFood().size() == getFood().size() - 1
	 * @post    This world has no longer the given food as
	 *      	one of its food.
	 *       	| ! new.hasAsFood(food)
	 * @post   	All food registered at an index beyond the index at
	 *        	which the given food was registered, are shifted
	 *       	one position to the left.
	 *       	| for each I,J in 0..getFood().size()-1:
	 *      	|   if ( (getFood().get(I) == food) and (I < J) )
	 *      	|     then new.getFood().get(J-1) == getFood().get(J)
	 * @throws	IllegalArgumentException
	 * 			An exception is thrown when the food doesn't exists or
	 * 			if this world has not the given food as one of its food or
	 * 			if the given food his world still exists.
	 * 			| (food == null) || !this.hasAsFood(food) || !(food.getWorld() == null)
	 */
	@Raw
	public void removeFood(Food food) throws IllegalArgumentException {
		if( (food == null) || !this.hasAsFood(food) || !(food.getWorld() == null) )
			throw new IllegalArgumentException("The provided food is not a valid food.");
		getFood().remove(food);
	}
	
	/**
	 * Variable referencing a list collecting all the food in this world.
	 */
	private ArrayList<Food> foodObjects = new ArrayList<Food>();
	
	//<-------------------------------------------------------------WORMS------------------------------------------------------------>
		
	/**
	 * A method to return a list of all the worms in this world.
	 */
	@Basic
	public ArrayList<Worm> getWorms() {
		return wormObjects;
	}
		
	/**
	 * Check whether this world can have the given worm
	 * as one of its worms.
	 * 
	 * @param	worm
	 *        	The worm to check.
	 * @return	True if and only if the given worm is effective
	 *        	and already references this world, and this world
	 *        	does not yet have the given worm as one of its worms.
	 *       	| result ==
	 *       	|   (worm != null) && (worm.getWorld() == this)
	 *			|		&& (!this.hasAsWorm(worm))
	 */
	@Raw
	public boolean canHaveAsWorm(Worm worm) {
		return (worm != null) && (worm.getWorld() == this)
				&& (!this.hasAsWorm(worm));
	}
		
	/**
	 * Check whether this world has the given worm as one of its worms.
	 * 
	 * @param  	worm
	 * 		   	The worm to check.
	 * @return	The result is true when the provided worm is a worm of this world.
	 *       	| result ==  getWorms().contains(worm)
	 */
	public boolean hasAsWorm(@Raw Worm worm) {
		return getWorms().contains(worm);
	}
		
	/**
	 * Add the given worm to the list of worms of this world.
	 * 
	 * @param	worm
	 *         	The worm to be added.
	 * @post	The number of worms of this world is
	 *        	incremented by 1.
	 *      	| new.getWorms().size() == this.getWorms().size() + 1
	 * @post   	This world has the given worm as its very last worm.
	 *       	| new.getWorms().get(getWorms().size()+1) == worm
	 * @throws	IllegalArgumentException
	 * 			An exception is thrown when the provided worm is not a valid worm.
	 * 			| ! canHaveAsWorm(Worm)
	 * @throws	IllegalArgumentException
	 * 			An exception is thrown when u want to add a worm if the game is started.
	 * 			| isGameStarted()
	 */
	@Basic
	public void addWorm(Worm worm) throws IllegalArgumentException{
		if(!canHaveAsWorm(worm))
			throw new IllegalArgumentException("The provided worm is not a valid worm.");
		if(isGameStarted())
			throw new IllegalArgumentException("The game is started u cannot add any worms.");
		wormObjects.add(worm);
	}
		
	/**
	 * 	A method to remove the given worm from the list of worms of this world.
	 * 
	 * @param	worm
	 *         	The worm to be removed.
	 * @post   	The number of worms of this world is
	 *         	decremented by 1.
	 *       	| new.getWorms().size() == getWorms().size() - 1
	 * @post    This world has no longer the given worm as
	 *      	one of its worms.
	 *       	| ! new.hasAsWorm(worm)
	 * @post   	All worms registered at an index beyond the index at
	 *        	which the given worm was registered, are shifted
	 *       	one position to the left.
	 *       	| for each I,J in 0..getWorms().size()-1:
	 *      	|   if ( (getWorms().get(I) == worm) and (I < J) )
	 *      	|     then new.getWorms().get(J-1) == getWorms().get(J)
	 * @throws	IllegalArgumentException
	 * 			An exception is thrown when the worm doesn't exists or
	 * 			if this world has not the given worm as one of its worms or
	 * 			if the given worm his world still exists.
	 * 			| (worm == null) || !this.hasAsWorm(worm) || !(worm.getWorld() == null)
	 */
	@Raw
	public void removeWorm(Worm worm) throws IllegalArgumentException {
		if( (worm == null) || !this.hasAsWorm(worm) || !(worm.getWorld() == null) )
			throw new IllegalArgumentException("The provided worm is not a valid worm.");
		getWorms().remove(worm);
	}
	
	/**
	 * Variable referencing a list collecting all the worms in this world.
	 */
	private ArrayList<Worm> wormObjects = new ArrayList<Worm>();
	
	//<---------------------------------------------------------RANDOM------------------------------------------------------->
 
	/**
	 * A method to return a random.
	 */
	public Random getRandom(){
		return random;
	}
	
	/**
	 * Variable registering the random of this world.
	 */
	private Random random;
	
	//<--------------------------------------------------------LOCATION------------------------------------------------------>
	//TODO

	/**
	 * A method that returns a rectangular matrix indicating which parts of the terrain are passable and impassable.
	 */
	@Basic
	public boolean[][] getPassable() {
		return passableMap ;
	}
	
	/**
	 * A method to check whether the given circular region of the given world,
	 * defined by the given center coordinates and radius,
	 * is passable and adjacent to impassable terrain. 
	 * 
	 * @param	xCoordinate
	 * 			The x-coordinate of the center of the circle to check.
	 * @param	yCoordinate
	 * 			The y-coordinate of the center of the circle to check.
	 * @param	radius
	 * 			The radius of the circle to check.
	 * @post	if the parameter xCoordinate is smaller than zero, the xCoordinate equals to
	 * 			the absolute value of xCoordinate.
	 * 			| if(xCoordinate < 0)
	 * 			| 	then xCoordinate = Math.abs(xCoordinate)
	 * @post	if the parameter yCoordinate is smaller than zero, the yCoordinate equals to
	 * 			the absolute value of yCoordinate.
	 * 			| if(yCoordinate < 0)
	 * 			| 	then yCoordinate = Math.abs(yCoordinate)
	 * @post	if the parameter xCoordinate is bigger than the width of the world, the xCoordinate equals to
	 * 			the width of the world.
	 * 			| if(xCoordinate > getWidth())
	 * 			|	then xCoordinate = getWidth()
	 * @post	if the parameter yCoordinate is bigger than the height of the world, the yCoordinate equals to
	 * 			the height of the world.
	 * 			| if(yCoordinate > getHeight())
	 * 			|	then yCoordinate = getHeight()
	 * @return	The result is true if the given region is passable and
	 * 			adjacent to impassable terrain, false otherwise.
	 * 			| result == isPassable(xCoordinate, yCoordinate, radius) && !isPassable(xCoordinate, yCoordinate, radius+radius*0.1)
	 */
	public boolean isAdjacentToImpassableTerrain(double xCoordinate, double yCoordinate, double radius){
		if(xCoordinate < 0)
			xCoordinate = Math.abs(xCoordinate);
		if(yCoordinate < 0)
			yCoordinate = Math.abs(yCoordinate);
		if(xCoordinate > getWidth())
			xCoordinate = getWidth();
		if(yCoordinate >getHeight())
			yCoordinate = getHeight();
		if( radius < 0);
			radius = Math.abs(radius);
		return isPassable(xCoordinate, yCoordinate, radius) && !isPassable(xCoordinate, yCoordinate, radius+(radius*0.1));
	}
	
	/**
	 * A method to check whether the given circular region of the given world,
	 * defined by the given center coordinates and radius,
	 * is impassable.
	 * 
	 * @param	xCoordinate
	 * 			The x-coordinate of the center of the circle to check.
	 * @param	yCoordinate
	 * 			The y-coordinate of the center of the circle to check.
	 * @param	radius
	 * 			The radius of the circle to check
	 * @post	if the parameter xCoordinate is smaller than zero, the xCoordinate equals to
	 * 			the absolute value of xCoordinate.
	 * 			| if(xCoordinate < 0)
	 * 			| 	then xCoordinate = Math.abs(xCoordinate)
	 * @post	if the parameter yCoordinate is smaller than zero, the yCoordinate equals to
	 * 			the absolute value of yCoordinate.
	 * 			| if(yCoordinate < 0)
	 * 			| 	then yCoordinate = Math.abs(yCoordinate)
	 * @post	if the parameter xCoordinate is bigger than the width of the world, the xCoordinate equals to
	 * 			the width of the world.
	 * 			| if(xCoordinate > getWidth())
	 * 			|	then xCoordinate = getWidth()
	 * @post	if the parameter yCoordinate is bigger than the height of the world, the yCoordinate equals to
	 * 			the height of the world.
	 * 			| if(yCoordinate > getHeight())
	 * 			|	then yCoordinate = getHeight()
	 * @return	The result is true if the given region is impassable, false otherwise.
	 * 			| result == !isPassable(xCoordinate, yCoordinate, radius)
	 */
	public boolean isImpassable(double xCoordinate, double yCoordinate, double radius){
		if(xCoordinate < 0)
			xCoordinate = Math.abs(xCoordinate);
		if(yCoordinate < 0)
			yCoordinate = Math.abs(yCoordinate);
		if(xCoordinate > getWidth())
			xCoordinate = getWidth();
		if(yCoordinate >getHeight())
			yCoordinate = getHeight();
		if( radius < 0);
			radius = Math.abs(radius);
		return !isPassable(xCoordinate, yCoordinate, radius);
	}
	
	/**
	 * A method to check whether the given circular region,
	 * defined by the given center coordinates and radius,
	 * is passable.
	 * 
	 * @param	xCoordinate
	 * 			The x-coordinate of the center of the circle to check.
	 * @param	yCoordinate
	 * 			The y-coordinate of the center of the circle to check.
	 * @param	radius
	 * 			The radius of the circle to check.
	 * @post	if the parameter xCoordinate is smaller than zero, the xCoordinate equals to
	 * 			the absolute value of xCoordinate.
	 * 			| if(xCoordinate < 0)
	 * 			| 	then xCoordinate = Math.abs(xCoordinate)
	 * @post	if the parameter yCoordinate is smaller than zero, the yCoordinate equals to
	 * 			the absolute value of yCoordinate.
	 * 			| if(yCoordinate < 0)
	 * 			| 	then yCoordinate = Math.abs(yCoordinate)
	 * @post	if the parameter xCoordinate is bigger than the width of the world, the xCoordinate equals to
	 * 			the width of the world.
	 * 			| if(xCoordinate > getWidth())
	 * 			|	then xCoordinate = getWidth()
	 * @post	if the parameter yCoordinate is bigger than the height of the world, the yCoordinate equals to
	 * 			the height of the world.
	 * 			| if(yCoordinate > getHeight())
	 * 			|	then yCoordinate = getHeight()
	 * @return	The result is true if the center coordinates are passable 
	 * 			and if the given circular region, defined by the given center 
	 * 			coordinates and radius, is passable.
	 * 			| if( getPassable()[(int)Math.round( (getPassable().length-1) - (yCoordinate * ((getPassable().length-1)/getHeight())) )]
	 * 			|				   [(int)Math.round( xCoordinate * ((getPassable()[0].length-1)/getWidth()))] )
	 * 			|	then for each I in 0..359:
	 * 			|			if( ! (getPassable()[(int)Math.round( (getPassable().length-1) - ( ((Math.sin(Math.toRadians(i))*radius) + yCoordinate) * ((getPassable().length-1)/getHeight())) )]
	 * 			|								[(int)Math.round( ((Math.cos(Math.toRadians(i))*radius) + xCoordinate) * ((getPassable()[0].length-1)/getWidth()))])
	 * 			|				then result == false;
	 * 			|		result == true;
	 * 			| else 
	 * 			|	result == false;
	 */
	public boolean isPassable(double xCoordinate, double yCoordinate, double radius){
		if(xCoordinate < 0)
			xCoordinate = Math.abs(xCoordinate);
		if(yCoordinate < 0)
			yCoordinate = Math.abs(yCoordinate);
		if(xCoordinate > getWidth())
			xCoordinate = getWidth();
		if(yCoordinate >getHeight())
			yCoordinate = getHeight();
		if( radius < 0);
			radius = Math.abs(radius);
		int row = (int)Math.round( (getPassable().length-1) - (yCoordinate * ((getPassable().length-1)/getHeight())) );
		int column = (int)Math.round( xCoordinate * ((getPassable()[0].length-1)/getWidth()));
		if( ( getPassable()[row][column] ) ){
			for(int i = 0; i < 360; i++ ){
				int tempRow = (int)Math.round( (getPassable().length-1) - ( ((Math.sin(Math.toRadians(i))*radius) + yCoordinate) * ((getPassable().length-1)/getHeight())) );
				int tempColumn = (int)Math.round( ((Math.cos(Math.toRadians(i))*radius) + xCoordinate) * ((getPassable()[0].length-1)/getWidth()));
				if(tempColumn < 0)
					tempColumn = Math.abs(tempColumn);
				if(tempRow < 0)
					tempRow = Math.abs(tempRow);
				if(tempColumn >= getPassable()[0].length)
					tempColumn = getPassable()[0].length-1;
				if(tempRow >= getPassable().length)
					tempRow = getPassable().length-1;
				if( !( getPassable()[tempRow][tempColumn] ) ){
					return false;
				}
			}
			return true;
		} else{
			return false;
		}
	}
	
	/**
	 * A method to find a random adjacent location.
	 * 
	 * @param	radius
	 * 			The radius to find a random adjacent location.
	 * @post	If the result is true and only if it is true,
	 * 			the new adjacentLocationX is set to founded adjacentLocationX
	 * 			| new.getAdjacentLocationX() == adjacentLocationX
	 * @post	If the result is true and only if it is true,
	 * 			the new adjacentLocationY is set to founded adjacentLocationY
	 * 			| new.getAdjacentLocationY() == adjacentLocationY
	 * @return	The result is true if a random perimeter location is found 
	 * 			and a random adjacent location is found.
	 * 			| if(findRandomPerimeterLocation())
	 *			| 	then double distanceBetweenWormAndMidle = Math.sqrt( Math.pow(this.getPerimeterLocationX() - (this.getWidth()/2), 2) +
	 *			|	  													 Math.pow(this.getPerimeterLocationY() - (this.getHeight()/2), 2) )
	 *			|		 double angleBetweenWormAndMidle = Math.atan( ((this.getHeight()/2) - this.getPerimeterLocationY()) / ((this.getWidth()/2) - this.getPerimeterLocationX()) )
	 *			| 		 double x = getPerimeterLocationX();
	 *			| 		 double y = getPerimeterLocationY();
	 *			| 		 if(getPerimeterLocationX()<getWidth()/2)
	 *			|			then if(angleBetweenWormAndMidle == 0.0)
	 *			|					then angleBetweenWormAndMidle = Math.PI
	 *			|				 while( !this.isAdjacentToImpassableTerrain(x, y, radius) && distanceBetweenWormAndMidle >= 0.5 )
	 *			|					 	x = x + (Math.cos(angleBetweenWormAndMidle)*0.01)
	 *			|						y = y + (Math.sin(angleBetweenWormAndMidle)*0.01)
	 *			|						distanceBetweenWormAndMidle = Math.sqrt( Math.pow(x - (this.getWidth()/2), 2) +
	 *			|											 					 Math.pow(y - (this.getHeight()/2), 2) )
	 *			|		if(getPerimeterLocationX()>getWidth()/2)
	 *			|			then angleBetweenWormAndMidle = angleBetweenWormAndMidle + Math.PI
	 *			|	   			 if(angleBetweenWormAndMidle == 0.0)
	 *			|					then angleBetweenWormAndMidle = Math.PI
	 *			|				 while( !this.isAdjacentToImpassableTerrain(x, y, radius) && distanceBetweenWormAndMidle >= 0.5)
	 *			|						x = x + (Math.cos(angleBetweenWormAndMidle)*0.01)
	 *			|						y = y + (Math.sin(angleBetweenWormAndMidle)*0.01)
	 *			|						distanceBetweenWormAndMidle = Math.sqrt( Math.pow(x - (this.getWidth()/2), 2) +
	 *			|											 					 Math.pow(y - (this.getHeight()/2), 2) );
	 *			|		if(this.isAdjacentToImpassableTerrain(x, y, radius))
	 *			|			adjacentLocationX = x;
	 *			|			adjacentLocationY = y;
	 *			|			return true;
	 */
	public boolean findRandomAdjacentLocation(double radius){
		if(findRandomPerimeterLocation()){
			double distanceBetweenWormAndMidle = Math.sqrt( Math.pow(this.getPerimeterLocationX() - (this.getWidth()/2), 2) +
					  										Math.pow(this.getPerimeterLocationY() - (this.getHeight()/2), 2) );
			double angleBetweenWormAndMidle = Math.atan( ((this.getHeight()/2) - this.getPerimeterLocationY()) / ((this.getWidth()/2) - this.getPerimeterLocationX()) );
			double x = getPerimeterLocationX();
			double y = getPerimeterLocationY();
			if(getPerimeterLocationX()<getWidth()/2){
				if(angleBetweenWormAndMidle == Math.PI)
					angleBetweenWormAndMidle = 0.0;
				while( !this.isAdjacentToImpassableTerrain(x, y, radius) && distanceBetweenWormAndMidle >= 0.5){
					x = x + (Math.cos(angleBetweenWormAndMidle)*0.01);
					y = y + (Math.sin(angleBetweenWormAndMidle)*0.01);
					distanceBetweenWormAndMidle = Math.sqrt( Math.pow(x - (this.getWidth()/2), 2) +
															 Math.pow(y - (this.getHeight()/2), 2) );
				}
			}
			if(getPerimeterLocationX()>getWidth()/2){
				angleBetweenWormAndMidle = angleBetweenWormAndMidle + Math.PI;
				if(angleBetweenWormAndMidle == 0.0)
					angleBetweenWormAndMidle = Math.PI;
				while( !this.isAdjacentToImpassableTerrain(x, y, radius) && distanceBetweenWormAndMidle >= 0.5){
					x = x + (Math.cos(angleBetweenWormAndMidle)*0.01);
					y = y + (Math.sin(angleBetweenWormAndMidle)*0.01);
					distanceBetweenWormAndMidle = Math.sqrt( Math.pow(x - (this.getWidth()/2), 2) +
															 Math.pow(y - (this.getHeight()/2), 2) );
				}
			}
			if(this.isAdjacentToImpassableTerrain(x, y, radius)){
				adjacentLocationX = x;
				adjacentLocationY = y;
				return true;
			}
			return findRandomAdjacentLocation(radius);
		}
		return findRandomAdjacentLocation(radius);
	}
	
	
	/**
	 * A method to find a random perimeter location.
	 * 
	 * @post	If the result is true and only if it is true,
	 * 			the new perimeterLocationX is set to founded perimeterLocationX
	 * 			| new.getPerimeterLocationX() == perimeterLocationX
	 * @post	If the result is true and only if it is true,
	 * 			the new perimeterLocationY is set to founded perimeterLocationY
	 * 			| new.getPerimeterLocationY() == perimeterLocationY
	 * @return	The result is true if a random perimeter location is found.
	 * 			| if(randomNumberBetweenOneAndFour != getOldRandomNumber())
	 *			| 	then if(randomNumberBetweenOneAndFour == 1)
	 *			|			then (perimeterLocationX = random.nextInt((int)getWidth()+1)) && (perimeterLocationY = 0.0) && result == true
	 *			|  	then if(randomNumberBetweenOneAndFour == 2)
	 *			|			then (perimeterLocationX = 0.0) && (perimeterLocationY = random.nextInt((int)getHeight()+1)) && result == true
	 *			|	then if(randomNumberBetweenOneAndFour == 3)
	 *			|       	then (perimeterLocationX = random.nextInt((int)getWidth()+1)) && (perimeterLocationY = getHeight()) && result == true
	 *			|	then if(randomNumberBetweenOneAndFour == 4)
	 *			|			then (perimeterLocationX = getWidth()) && (perimeterLocationY = random.nextInt((int)getHeight()+1)) && result == true
	 *			|	then setOldRandomNumber(randomNumberBetweenOneAndFour)
	 *			| else 
	 *			|	findRandomPerimeterLocation()
	 *			| result == true
	 */
	private boolean findRandomPerimeterLocation(){
		int randomNumberBetweenOneAndFour = getRandom().nextInt(4) + 1;
		if(randomNumberBetweenOneAndFour != getOldRandomNumber()){
			if(randomNumberBetweenOneAndFour == 1){
				perimeterLocationX = getRandom().nextInt((int)getWidth()+1);
				perimeterLocationY = 0.0;
				return true;
			}
			if(randomNumberBetweenOneAndFour == 2){
				perimeterLocationX = 0.0;
				perimeterLocationY = getRandom().nextInt((int)getHeight()+1);
				return true;
			}
			if(randomNumberBetweenOneAndFour == 3){
				perimeterLocationX = getRandom().nextInt((int)getWidth()+1);
				perimeterLocationY = getHeight();
				return true;
			}
			if(randomNumberBetweenOneAndFour == 4){
				perimeterLocationX = getWidth();
				perimeterLocationY = getRandom().nextInt((int)getHeight()+1);
				return true;
			}
			setOldRandomNumber(randomNumberBetweenOneAndFour);
		} else {
			findRandomPerimeterLocation();
		}
		return true;
	}
	
	/**
	 * A method that returns the x-coordinate of a adjacent location.
	 */
	@Basic
	public double getAdjacentLocationX(){
		return adjacentLocationX;
	}
	
	/**
	 * A method that returns the y-coordinate of a adjacent location. 
	 */
	@Basic
	public double getAdjacentLocationY(){
		return adjacentLocationY;
	}
	
	/**
	 * A method that returns the x-coordinate of a perimeter location. 
	 */
	private double getPerimeterLocationX(){
		return perimeterLocationX;
	}
	
	/**
	 * A method that returns the y-coordinate of a perimeter location. 
	 */
	private double getPerimeterLocationY(){
		return perimeterLocationY;
	}
	
	/**
	 * A method that returns the old random number. 
	 */
	private int getOldRandomNumber() {
		return oldRandomNumber;
	}

	/**
	 * A method to set the old random number. 
	 * 
	 * @param	oldRandomNumber
	 * 			The provided oldRandomNumber.
	 * @post	The new oldRandomNumber is set to the provided oldRandomNumber.
	 * 			| new.getOldRandomNumber = oldRandomNumber
	 */
	private void setOldRandomNumber(int oldRandomNumber) {
		this.oldRandomNumber = oldRandomNumber;
	}
	
	/**
	 * Variable registering the old random number of this world.
	 */
	private int oldRandomNumber = 0;
	
	/**
	 * Variable registering the y-coordinate of the adjacent location.
	 */
	private double adjacentLocationY;
	
	/**
	 * Variable registering the x-coordinate of the adjacent location.
	 */
	private double adjacentLocationX;
	
	/**
	 * Variable registering the x-coordinate of the perimeter location.
	 */
	private double perimeterLocationX;
	
	/**
	 * Variable registering the y-coordinate of the adjacent location.
	 */
	private double perimeterLocationY;
	
	/**
	 * Variable registering a rectangular matrix indicating which parts of the terrain are passable and impassable.
	 */
	private boolean[][] passableMap;
	
	//<----------------------------------------------------------PROJECTILES---------------------------------------------------------->
	
	/**
	 * A method to return a list of all the projectiles in this world.
	 */
	@Basic
	public ArrayList<Projectile> getProjectiles() {
		return projectileObjects;
	}
	
	/**
	 * A method to return the active projectile in this world, or null if no active projectile exists.
	 */
	@Basic
	public Projectile getActiveProjectile(){
		return getProjectiles().get(0);
	}
		
	/**
	 * Check whether this world can have the given projectile
	 * as one of its projectiles.
	 * 
	 * @param	projectile
	 *        	The projectile to check.
	 * @return	True if and only if the given projectile is effective
	 *        	and already references this world, and this world
	 *        	does not yet have the given projectile as one of its projectiles.
	 *       	| result ==
	 *       	|   (projectile != null) && (projectile.getWorld() == this)
	 *			|		&& (!this.hasAsProjectile(projectile))
	 */
	@Raw
	public boolean canHaveAsProjectile(Projectile projectile) {
		return (projectile != null) && (projectile.getWorld() == this)
				&& (!this.hasAsProjectile(projectile));
	}
		
	/**
	 * Check whether this world has the given projectile as one of its projectiles.
	 * 
	 * @param  	projectile
	 * 		   	The projectile to check.
	 * @return	The result is true when the provided projectile is a projectile of this world.
	 *       	| result ==  getProjectiles().contains(projectile)
	 */
	public boolean hasAsProjectile(@Raw Projectile projectile) {
		return getProjectiles().contains(projectile);
	}
		
	/**
	 * Add the given projectile to the list of projectiles of this world.
	 * 
	 * @param	projectile
	 *         	The projectile to be added.
	 * @post	The number of projectiles of this world is
	 *        	incremented by 1.
	 *      	| new.getProjectiles().size() == this.getProjectiles().size() + 1
	 * @post   	This world has the given projectile as its very last projectile.
	 *       	| new.getProjectiles().get(getProjectiles().size()+1) == projectile
	 * @throws	IllegalArgumentException
	 * 			An exception is thrown when the provided projectile is not a valid projectile.
	 * 			| ! canHaveAsProjectile(Projectile)
	 */
	@Basic
	public void addProjectile(Projectile projectile) throws IllegalArgumentException{
		if(!canHaveAsProjectile(projectile))
			throw new IllegalArgumentException("The provided projectile is not a valid projectile.");
		projectileObjects.add(projectile);
	}
		
	/**
	 * 	A method to remove the given projectile from the list of projectiles of this world.
	 * 
	 * @param	projectile
	 *         	The projectile to be removed.
	 * @post   	The number of projectiles of this world is
	 *         	decremented by 1.
	 *       	| new.getProjectiles().size() == getProjectiles().size() - 1
	 * @post    This world has no longer the given projectile as
	 *      	one of its projectiles.
	 *       	| ! new.hasAsProjectile(projectile)
	 * @post   	All projectiles registered at an index beyond the index at
	 *        	which the given projectile was registered, are shifted
	 *       	one position to the left.
	 *       	| for each I,J in 0..getProjectiles().size()-1:
	 *      	|   if ( (getProjectiles().get(I) == projectile) and (I < J) )
	 *      	|     then new.getProjectiles().get(J-1) == getProjectiles().get(J)
	 * @throws	IllegalArgumentException
	 * 			An exception is thrown when the projectile doesn't exists or
	 * 			if this world has not the given projectile as one of its projectiles or
	 * 			if the given projectile his world still exists.
	 * 			| (projectile == null) || !this.hasAsProjectile(projectile) || !(projectile.getWorld() == null)
	 */
	@Raw
	public void removeProjectile(Projectile projectile) throws IllegalArgumentException {
		if( (projectile == null) || !this.hasAsProjectile(projectile) || !(projectile.getWorld() == null) )
			throw new IllegalArgumentException("The provided projectile is not a valid projectile.");
		getProjectiles().remove(projectile);
	}
	
	/**
	 * Variable referencing a list collecting all the projectiles in this world.
	 */
	private ArrayList<Projectile> projectileObjects = new ArrayList<Projectile>();
	
	//<---------------------------------------------------------------GAME------------------------------------------------------------>
	
	/**
	 * A method to check whether the game is finished.
	 * 
	 * @return	If there is only one worm alive then the game
	 * 			is finished.
	 * 			| result == (getWorms().size() == 1)
	 * @return 	If there are more than one worm alive but 
	 * 			they are in the same team then the game is
	 * 			finished.
	 * 			| for each I in 0..getWorms().size()-1:
	 * 			|	if( getWorms().get(0).getTeam() != getWorms().get(i).getTeam() )
	 * 			|		then result == false
	 * 			| result == true
	 */
	public boolean isGameFinished(){
		if(getWorms().size() == 1)
			return true;
		for(int i = 0; i < getWorms().size(); i++){
			if(getWorms().get(0).getTeam() != null){
				if(getWorms().get(0).getTeam() != getWorms().get(i).getTeam()){
					return false;
				}
			} else {
				return false;
			}
		}
		return true;
	}
	
	/**
	 * A method to start a new game.
	 */
	public void startGame() {
		if(!(getWorms().size() < 2)){
			setGameStarted(true);
			setCurrentWorm(getWorms().get(0));
		}
	}
	
	/**
	 * A method to start the next turn in this world.
	 * 
	 * @post	If the action points of the current worm is lower or equal to zero,
	 * 			then the next worm will be selected. If the current worm was the last worm
	 * 			of the worms of this world, then the first worm will be selected. 
	 * 			| if(getCurrentWorm().getCurrentActionPoints() <= 0)
	 * 			|	then if(getWorms().indexOf(getCurrentWorm())+1 >= getWorms().size())
	 * 			|			setCurrentWorm(getWorms().get(0))
	 * @post	If the action points of the current worm is lower or equal to zero,
	 * 			then the next worm will be selected. If the current worm was not the last worm
	 * 			of the worms of this world, then the next worm will be selected. 
	 * 			| if(getCurrentWorm().getCurrentActionPoints() <= 0)
	 * 			|	then if( !(getWorms().indexOf(getCurrentWorm())+1 >= getWorms().size()))
	 * 			|			setCurrentWorm(getWorms().get(getWorms().indexOf(getCurrentWorm())+1))
	 * @post 	The new selected worm, his action points are set to the maximum and his hit points
	 * 			are increased by 10.
	 * 			| new.getCurrentWorm().setCurrentActionPoints(getCurrentWorm().getMaximumHitPoints())
	 * 			| new.getCurrentWorm().setCurrentHitPoints(getCurrentWorm().getCurrentHitPoints()+10)
	 */
	public void startNextTurn(){
		if(getCurrentWorm().getCurrentActionPoints() <= 0){
			if(getWorms().indexOf(getCurrentWorm())+1 >= getWorms().size()){
				setCurrentWorm(getWorms().get(0));
				getCurrentWorm().setCurrentActionPoints(getCurrentWorm().getMaximumHitPoints());
				getCurrentWorm().setCurrentHitPoints(getCurrentWorm().getCurrentHitPoints()+10);
			}
			else{
				setCurrentWorm(getWorms().get(getWorms().indexOf(getCurrentWorm())+1));
				getCurrentWorm().setCurrentActionPoints(getCurrentWorm().getMaximumHitPoints());
				getCurrentWorm().setCurrentHitPoints(getCurrentWorm().getCurrentHitPoints()+10);
			}
		}
	}
	
	/**
	 * A method to return the name of the worm of the winner of this game
	 * or the team name of the worms of the winners of this game.
	 */
	public String getWinner() {
		if(getWorms().get(0).getTeam() != null)
			return getWorms().get(0).getTeam().getName();
		else {
			return getWorms().get(0).getName();
		}
	}
	
	/**
	 * A method to return the boolean gameStarted, if the game is started true else false. 
	 */
	public boolean isGameStarted() {
		return gameStarted;
	}
	
	/**
	 * A method to set the boolean gameStarted.
	 * 
	 * @param 	gameStarted
	 * 			The new value for the boolean gamestarted.	
	 * @post	The boolean gameStarted is set to the provided gameStarted.
	 * 			| new.isGameStarted() == gameStarted
	 */
	public void setGameStarted(boolean gameStarted) {
		this.gameStarted = gameStarted;
	}
	
	/**
	 * A method to return the active worm in this world.
	 */
	public Worm getCurrentWorm(){
		return CurrentWorm;
	}
	
	/**
	 * A method to set the current worm.
	 * 
	 * @param 	newCurrentWorm
	 * 			The new current worm.
	 * @post	The current worm is set to the provided worm.
	 * 			| new.getCurrentWorm() == newCurrentWorm
	 */
	public void setCurrentWorm(Worm newCurrentWorm) {
		this.CurrentWorm = newCurrentWorm;
	}

	/**
	 * Variable registering the boolean gameStarted of this world.
	 */
	private boolean gameStarted;
	
	/**
	 * Variable registering the currentWorm of this world.
	 */
	private Worm CurrentWorm;
}
