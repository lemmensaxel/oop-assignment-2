package worms.model;

import be.kuleuven.cs.som.annotate.Basic;
import be.kuleuven.cs.som.annotate.Raw;

/**
 * A class of Bazooka's, a subclass of Weapon.
 *
 * @version  2.0
 * @author   Axel Lemmens & Maarten Rimaux
 */
public class Bazooka extends Weapon {

	public Bazooka(Worm worm) throws IllegalArgumentException {
		super(worm);
	}
	
	/**
	 * Check whether this Bazooka can have the given worm as its current worm.
	 * 
	 * @param	worm
	 *          The worm to check.
	 * @return	True if and only if the given worm is effective and if the worm
	 * 			doesn't have a weapon of this class.
	 *       	|   result == (worm != null) && (!worm.hasAsWeapon(this))
	 */
	@Override
	@Raw
	public boolean canHaveAsWorm(Worm worm) {
			return (worm != null) && (!worm.hasAsWeapon(this));
	}

	/**
	 * Check whether this Bazooka can have the given projectile
	 * as one of its projectiles.
	 * 
	 * @param	projectile
	 *        	The projectile to check.
	 * @return	True if and only if the given projectile is effective
	 *        	and already references this Bazooka, and this Bazooka
	 *        	does not yet have the given projectile as one of its projectiles
	 *         	and the given projectile is of the class BazookaBullet.
	 *       	| result ==
	 *       	|   (projectile != null) 
	 *       	|		&& (projectile.getWeapon() == this)
	 *			|		&& (!this.hasAsProjectile(projectile))
	 *			|		&& (projectile instanceof BazookaBullet)
	 */
	@Raw
	public boolean canHaveAsProjectile(Projectile projectile) {
		return (projectile != null) 
				&& (projectile.getWeapon() == this)
				&& (!this.hasAsProjectile(projectile)) 
				&& (projectile instanceof BazookaBullet);
	}
	
	/**
	 * A method to return the name of this Bazooka.
	 */
	@Override
	@Basic
	public String getName() {
		return "Bazooka";
	}

}
